exports.welcome = (data) => {
	return {
		en: {
			subject: `Welcome ${data.name}!`,
			title: 'Welcome to the community.',
			subtitle: "We're glad to have you here. We hope your adventure starts now and you have a great time.",
			adventures: 'Adventures',
			confirm_account: 'Confirm account',
			footer_top: `You're receiving this email because your Doodlay account is associated to this email address. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Privacy policy</a>`,
			footer_bottom: 'Sent with ♥ from Doodlay'
		},
		es: {
			subject: `¡Bienvenido ${data.name}!`,
		    title: 'Bienvenido a la comunidad.',
		    subtitle: 'Estamos encantados de tenerte aquí. Esperamos que ahora empiece tu aventura y lo pases en grande.',
		    adventures: 'Aventuras',
		    confirm_account: 'Confirmar cuenta',
			footer_top: `Has recibido este mensaje porque tu cuenta de Doodlay está asociada a esta dirección de correo electrónico. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Política de privacidad</a>`,
		    footer_bottom: 'Enviado con ♥ desde Doodlay'
		}
	};
};

exports.confirm = (data) => {
	return {
		en: {
			subject: `Welcome ${data.name}!`,
			title: 'Welcome to the community.',
			subtitle: "We're glad to have you here. We hope your adventure starts now and you have a great time.",
			adventures: 'Adventures',
			verify_account: 'Verify my account',
			footer_top: `You're receiving this email because your Doodlay account is associated to this email address. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Privacy policy</a>`,
			footer_bottom: 'Sent with ♥ from Doodlay'
		},
		es: {
			subject: `¡Bienvenido ${data.name}!`,
			title: 'Bienvenido a la comunidad.',
		    subtitle: 'Estamos encantados de tenerte aquí. Esperamos que ahora empiece tu aventura y lo pases en grande.',
		   	adventures: 'Adventures',
		   	verify_account: 'Confirmar mi cuenta',
			footer_top: `Has recibido este mensaje porque tu cuenta de Doodlay está asociada a esta dirección de correo electrónico. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Política de privacidad</a>`,
		    footer_bottom: 'Enviado con ♥ desde Doodlay'
		}
	};
};

exports.reset = (data) => {
	return {
		en: {
			subject: `Welcome ${data.name}!`,
			title: 'Change your password.',
			subtitle: "It looks like there's a problem with the password. Let's solve it.",
			adventures: 'Adventures',
			account: 'You have requested a password change, in the following link you can update your account with a new one. If it was not you, please change it for security or contact us.',
			reset_account: 'Change password',
			footer_top: `You're receiving this email because your Doodlay account is associated to this email address. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Privacy policy</a>`,
			footer_bottom: 'Sent with ♥ from Doodlay'
		},
		es: {
			subject: `¡Bienvenido ${data.name}!`,
			title: 'Cambia tu contraseña.',
		    subtitle: 'Vaya, parece que hay un problema con la contraseña. Vamos a resolverlo.',
		   	adventures: 'Adventures',
		   	account: 'Has solicitado un cambio de contraseña, en el siguiente enlace podrás actualizar tu cuenta con una nueva. Si no has sido tu, por favor, cámbiala a modo de seguridad o contacta con nostros.',
		   	reset_account: 'Cambiar contraseña',
			footer_top: `Has recibido este mensaje porque tu cuenta de Doodlay está asociada a esta dirección de correo electrónico. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Política de privacidad</a>`,
		    footer_bottom: 'Enviado con ♥ desde Doodlay'
		}
	};
};

exports.support = (data) => {
	return {
		en: {
			subject: `New support message from ${data.name}!`,
			title: `New support message from ${data.name} (${data.from }):`,
			adventures: 'Adventures',
			message: data.message,
			footer_top: `You're receiving this email because your Doodlay account is associated to this email address. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Privacy policy</a>`,
			footer_bottom: 'Sent with ♥ from Doodlay'
		},
		es: {
			subject: `Nuevo mensaje de soporte de ${data.name}!`,
			title: `Nuevo mensaje de soporte de ${data.name} (${data.from }):`,
			adventures: 'Adventures',
			message: data.message,
			footer_top: `Has recibido este mensaje porque tu cuenta de Doodlay está asociada a esta dirección de correo electrónico. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Política de privacidad</a>`,
		    footer_bottom: 'Enviado con ♥ desde Doodlay'
		}
	};
};

exports.delete = (data) => {
	return {
		en: {
			subject: 'Delete account',
			title: 'We are sorry for your loss.',
			subtitle: `We will be happy to have you here again ${data.name}.`,
			adventures: 'Adventures',
			delete_account: 'Delete my account',
			footer_top: `You're receiving this email because your Doodlay account is associated to this email address. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Privacy policy</a>`,
			footer_bottom: 'Sent with ♥ from Doodlay'
		},
		es: {
			subject: 'Borrar cuenta',
			title: 'Sentimos que te vayas.',
			subtitle: `Estaremos encantados de tenerte aquí otra vez ${data.name}.`,
			adventures: 'Adventures',
			delete_account: 'Borrar mi cuenta',
			footer_top: `Has recibido este mensaje porque tu cuenta de Doodlay está asociada a esta dirección de correo electrónico. <a href="https://doodlay.co/terms/privacy_policy" target="_blank" style="text-decoration:underline;color:#d8d8d8;font-family:Helvetica,Arial,sans-serif;">Política de privacidad</a>`,
		    footer_bottom: 'Enviado con ♥ desde Doodlay'
		}
	};
};