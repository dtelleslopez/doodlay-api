module.exports = {
	'HEADER_NOT_PROVIDED': 'Header authorization not provided',
	'NOT_FOUND': 'Not found',
	'EMAIL_IN_USE': 'Email address already in use',
	'ERROR_CREATE_USER': 'Error creating user',
	'EMAIL_IN_USE_WITH_PROVIDER': 'Email address already in use with provider ',
	'INCORRECT_PASSWORD': 'Incorrect password',
	'ERROR_CREATE_ACTIVITY': 'Error creating activity',
	'ERROR_UPDATE_ACTIVITY': 'Error updating activity',
	'ERROR_GET_ACTIVITY': 'Error getting activity',
	'ERROR_GET_ACTIVITIES': 'Error getting activities',
	'ERROR_GET_ACTIVITIES_LOCATIONS': 'Error getting activities locations',
	'ERROR_GET_LEVELS': 'Error getting levels',
	'ERROR_INVITE_ACTIVITY': 'Error inviting activity',
	'ERROR_JOIN_ACTIVITY': 'Error joining activity',
	'ERROR_LEAVE_ACTIVITY': 'Error leaving activity',
	'ERROR_ACCEPT_ACTIVITY': 'Error accepting activity',
	'ERROR_REJECT_ACTIVITY': 'Error rejecting activity',
	'ERROR_CANCEL_ACTIVITY': 'Error canceling activity',
	'ERROR_DELETE_ACTIVITY': 'Error deleting activity',
	'ERROR_SEND_MESSAGE': 'Error sending message',
	'ERROR_GET_IMAGE': 'Error getting image',
	'ERROR_UPLOAD_IMAGE': 'Error uploading image',
	'ERROR_JOIN_FRIEND': 'Error joining friend',
	'ERROR_ACCEPT_FRIEND': 'Error accepting friend',
	'ERROR_CANCEL_FRIEND': 'Error canceling friend',
	'ERROR_REJECT_FRIEND': 'Error rejecting friend',
	'ERROR_DELETE_FRIEND': 'Error deleting friend',
	'ERROR_GET_TAGS': 'Error getting tags',
	'ERROR_GET_PROFILE': 'Error getting profile',
	'ERROR_UPDATE_PROFILE': 'Error updating profile',
	'ERROR_UPDATE_DEVICE': 'Error updating device',
	'ERROR_UPDATE_LOCATION': 'Error updating location',
	'ERROR_SEND_SUPPORT': 'Error sending support',
	'ERROR_GET_WALL': 'Error getting wall',
	'TOKEN_NOT_VALID': 'Token not valid',
	'TOKEN_EXPIRED': 'Token expired',
	'ERROR_PENDING_ACTIVITY': 'Error getting pending activity',
	'ERROR_CONFIRM_EMAIL': 'Error confirm email',
	'ERROR_RESET_PASSWORD': 'Error reset password',
	'ERROR_CHANGE_PASSWORD': 'Error changing password',
	'ERROR_FORGOT_PASSWORD': 'Error forgot password',
	'ERROR_ACCEPT_TERMS': 'Error accept terms',
	'ERROR_CREATE_REQUEST': 'Error create request',
	'ERROR_DELETE_USER': 'Error delete user',
	'ERROR_CONFIRM_DELETE': 'Error confirm delete'
};