module.exports = {
	en: {
		'2017-01-01T00:00:00.000Z': 'You must accept the new terms and conditions to continue using Doodlay.',
		'2018-01-19T00:00:00.000Z': 'You must accept the new terms and conditions to continue using Doodlay.'
	},
	es: {
		'2017-01-01T00:00:00.000Z': 'Debes aceptar los nuevos términos y condiciones para seguir utilizando Doodlay.',
		'2018-01-19T00:00:00.000Z': 'Debes aceptar los nuevos términos y condiciones para seguir utilizando Doodlay.'
	}
};