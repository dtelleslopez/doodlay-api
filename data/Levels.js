module.exports = {
	en: {
		beginner: 'Beginner',
		occasional: 'Occasional',
		regular: 'Regular',
		expert: 'Expert'
	},
	es: {
		beginner: 'Principiante',
		occasional: 'Ocasional',
		regular: 'Asiduo',
		expert: 'Experto'
	}
};