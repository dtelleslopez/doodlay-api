const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const Relation = require('../../models/Relation');
const checkParams = require('../../helpers/check-params');
const Constants = require('../../data/Constants');
const notifications = require('../../helpers/notifications');

exports.joinFriend = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    if (String(req.params._id) === String(req.user._id)) return next(new CustomError(Constants.ERROR_JOIN_FRIEND, null, err));

    Relation.findOne({
        $and: [{
            $or: [{
                to: req.user._id,
                from: req.params._id
            }, {
                to: req.params._id,
                from: req.user._id
            }]
        }]
    }).exec((err, relation) => {
        if (err) return next(new CustomError(Constants.ERROR_JOIN_FRIEND, null, err));
        else if (!relation) {
            relation = new Relation({
                from: req.user._id,
                to: req.params._id
            });
        } else {
            switch (relation.status) {
                case config.relation.status.accepted:
                case config.relation.status.pending:
                    break;
                default:
                    relation.to = req.params._id;
                    relation.from = req.user._id;
                    relation.status = config.relation.status.pending;
                    break;
            }
        }

        relation.save((err, relation) => {
            if (err) return next(new CustomError(Constants.ERROR_JOIN_FRIEND, null, err));
            let data = {
                url: config.notifications.endpoint.user_join,
                notification: {
                    to: req.params._id,
                    from: req.user._id
                }
            };

            notifications.sendPostRequest(data.url, data.notification).then(() => {
                return res.status(200).send({});
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_INVITE_ACTIVITY, null, err));
            });
        });
    });
};

exports.acceptFriend = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Relation.findOne({
        to: req.user._id,
        from: req.params._id,
        status: config.relation.status.pending
    }).exec((err, relation) => {
        if (err || !relation) return next(new CustomError(Constants.ERROR_ACCEPT_FRIEND, 404, err));
        else {
            relation.status = config.relation.status.accepted;
            relation.save(() => {
                if (err) return next(new CustomError(Constants.ERROR_ACCEPT_FRIEND, null, err));

                let data = {
                    url: config.notifications.endpoint.user_accept,
                    notification: {
                        to: req.user._id,
                        from: req.params._id
                    }
                };

                notifications.sendPostRequest(data.url, data.notification).then(() => {
                    return res.status(200).send(relation);
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_INVITE_ACTIVITY, null, err));
                });
            });
        }
    });
};

exports.cancelFriend = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Relation.findOne({
        to: req.params._id,
        from: req.user._id
    }).exec((err, relation) => {
        if (err || !relation) return next(new CustomError(Constants.ERROR_CANCEL_FRIEND, 404, err));
        else {
            relation.status = config.relation.status.canceled;
            relation.save(() => {
                if (err) return next(new CustomError(Constants.ERROR_CANCEL_FRIEND, null, err));
                return res.status(200).send(relation);
            });
        }
    });
};

exports.rejectFriend = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Relation.findOne({
        to: req.user._id,
        from: req.params._id,
        status: config.relation.status.pending
    }).exec((err, relation) => {
        if (err || !relation) return next(new CustomError(Constants.ERROR_REJECT_FRIEND, 404, err));
        else {
            relation.status = config.relation.status.rejected;
            relation.save(() => {
                if (err) return next(new CustomError(Constants.ERROR_REJECT_FRIEND, null, err));
                return res.status(200).send(relation);
            });
        }
    });
};

exports.deleteFriend = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Relation.findOne({
        to: { $in: [req.user._id, req.params._id] },
        from: { $in: [req.user._id, req.params._id] },
        status: config.relation.status.accepted
    }).exec((err, relation) => {
        if (err || !relation) return next(new CustomError(Constants.ERROR_DELETE_FRIEND, 404, err));
        else {
            relation.status = config.relation.status.deleted;
            relation.save(() => {
                if (err) return next(new CustomError(Constants.ERROR_DELETE_FRIEND, null, err));
                return res.status(200).send(relation);
            });
        }
    });
};