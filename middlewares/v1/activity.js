const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const Activity = require('../../models/Activity');
const Image = require('../../models/Image');
const User = require('../../models/User');
const Tag = require('../../models/Tag');
const Level = require('../../models/Level');
const Wall = require('../../models/Wall');
const Constants = require('../../data/Constants');
const Levels = require('../../data/Levels');
const checkParams = require('../../helpers/check-params');
const notifications = require('../../helpers/notifications');
const Norris = require('../../helpers/norris');
const Locale = require('../../helpers/locale');

exports.createActivity = (req, res, next) => {
    let params = checkParams(req.body, ['name', 'description', 'date', 'level', 'longitude', 'latitude', 'private']);
    if (params) return next(new CustomError(params.message, params.error));

    let activity = new Activity({
        name: capitalize(req.body.name.trim()),
        description: req.body.description.trim(),
        date: req.body.date,
        level: req.body.level,
        user: req.user._id,
        location: { coordinates: [req.body.longitude, req.body.latitude] },
        private: req.body.private,
        members: { accepted: [{ user: req.user._id }] }
    });

    activity.save(activity, (err, activity) => {
        Tag.createMultipleActivity(req.body.tags, activity).then((tags) => {
            activity.tags = tags;
            Image.uploadBase64Image('activities/' + activity._id + '/original/', req.body.image).then((image) => {
                if (image) activity.image = image._id;
                activity.save((err, activity) => {
                    if (err) return next(new CustomError(Constants.ERROR_CREATE_ACTIVITY, null, err));
                    Wall.createOrUpdateActivity(activity, true).then(() => {
                        Norris(config.norris.create_activity, activity);
                        return res.status(200).send({ activity: activity });
                    }).catch((err) => {
                        return next(new CustomError(Constants.ERROR_CREATE_ACTIVITY, null, err));
                    });
                });
            }).catch((err) => {
                if (err) return next(new CustomError(Constants.ERROR_CREATE_ACTIVITY, null, err));
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_CREATE_ACTIVITY, null, err));
        });
    });
};

exports.updateActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.getActivityById(req.params._id).then((activity) => {
        if (req.body.name) activity.name = capitalize(req.body.name.trim());
        if (req.body.description) activity.description = req.body.description.trim();
        if (req.body.date) activity.date = req.body.date;
        if (req.body.level) activity.level = req.body.level;
        if (req.body.private) activity.private = req.body.private;
        if (req.body.latitude && req.body.longitude) activity.location.coordinates = [req.body.longitude, req.body.latitude];

        activity.save(activity, (err, activity) => {
            Tag.createMultipleActivity(req.body.tags, activity).then((tags) => {
                activity.tags = tags;
                if (req.body.image) {
                    Image.uploadBase64Image('activities/' + activity._id + '/original/', req.body.image).then((image) => {
                        activity.image = image;
                        activity.save((err, user) => {
                            if (err) return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
                            Wall.createOrUpdateActivity(activity).then(() => {
                                return res.status(200).send({ activity: activity });
                            }).catch((err) => {
                                return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
                            });
                        });
                    }).catch((err) => {
                        if (err) return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
                    });
                } else {
                    activity.save((err, user) => {
                        if (err) return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
                        Wall.createOrUpdateActivity(activity).then(() => {
                            return res.status(200).send({ activity: activity });
                        }).catch((err) => {
                            return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
                        });
                    });
                }
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
            });
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_UPDATE_ACTIVITY, null, err));
    });
};

exports.getLevels = (req, res, next) => {
    let lng = Locale(req.user.locale);

    Level.find().exec((err, levels) => {
        if (err) return next(new CustomError(Constants.ERROR_GET_LEVELS, null, err));
        else {
            levels = levels.map((level) => {
                level.name = Levels[lng][level.name];
                return level;
            });

            return res.status(200).send({ levels: levels  });
        }
    });
};

exports.getShareActivity = (req, res, next) => {
    let params = checkParams(req.params, ['share']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findOne({
        share: req.params.share
    }).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_GET_ACTIVITY, 404, err));
        else {
            return res.status(200).send({ 
                activity: {
                    name: activity.name,
                    description: activity.description,
                    image: activity.image
                }
            });
        }
    });
};

exports.getActivityByShare = (req, res, next) => {
    let params = checkParams(req.params, ['share']);
    if (params) return next(new CustomError(params.message, params.error));

    req.user.getFriends().then((relations) => {
        relations = relations.map((relation) => {
            if (String(relation.to) === String(req.user._id)) return relation.from;
            else return relation.to;
        });

        Activity.findOne({
                share: req.params.share,
                deleted: false,
                $and: [{
                    $or: [{
                        private: false
                    }, {
                        private: true,
                        $and: [{
                            $or: [{
                                'members.accepted.user': req.user
                            }, {
                                'members.invited.to': req.user
                            }, {
                                user: { $in: relations }
                            }, {
                                user: req.user
                            }]
                        }]
                    }]
                }]
            })
            .select('name date description user tags image level near location members private share')
            .populate('user level tags members.accepted.user members.pending.user members.rejected.user members.invited.to members.invited.from')
            .exec((err, activity) => {
                if (err || !activity) return next(new CustomError(Constants.ERROR_GET_ACTIVITY, 404, err));
                else {
                    let isAccepted = activity.isAccepted(req.user);
                    let isInvited = activity.isInvited(req.user);
                    let isPending = activity.isPending(req.user);

                    activity = activity.toJSON(['level', 'location', 'near', 'members']);
                    activity.user = User.toJSON(activity.user);

                    activity.members.accepted = activity.members.accepted.map((member) => {
                        member.user = User.toJSON(member.user);
                        return member;
                    });

                    if (String(activity.user._id) === String(req.user._id)) {
                        delete activity.near;

                        activity.members.pending = activity.members.pending.map((member) => {
                            member.user = User.toJSON(member.user);
                            return member;
                        });

                        activity.members.accepted = activity.members.accepted.map((member) => {
                            member.user = User.toJSON(member.user);
                            return member;
                        });

                        delete activity.members.invited;
                    } else {
                        if (isAccepted) delete activity.near;
                        else delete activity.location;

                        if (activity.members.accepted.length) {
                            activity.members.accepted = activity.members.accepted.map((member) => {
                                member.user = User.toJSON(member.user);
                                return member;
                            });
                        } else delete activity.members.accepted;

                        if (isInvited) {
                            activity.members.invited = activity.members.invited.filter((member) => {
                                if (String(member.to._id) === String(req.user._id)) {
                                    member.to = User.toJSON(member.to);
                                    member.from = User.toJSON(member.from);
                                    return member;
                                }
                            });
                        } else delete activity.members.invited;

                        if (isPending) {
                            activity.members.pending = activity.members.pending.filter((member) => {
                                if (String(member.user._id) === String(req.user._id)) {
                                    member.user = User.toJSON(member.user);
                                    return member;
                                }
                            });
                        } else delete activity.members.pending;
                    }

                    if (!activity.members.accepted && !activity.members.invited && !activity.members.pending) delete activity.members;
                    else delete activity.members.rejected;

                    activity.tags = activity.tags.map((tag) => {
                        return Tag.toJSON(tag);
                    });

                    let lng = Locale(req.user.locale);
                    activity.level.name = Levels[lng][activity.level.name];

                    return res.status(200).send({ activity: activity });
                }
            });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_ACTIVITY, null, err));
    });
};

exports.getActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    req.user.getFriends().then((relations) => {
        relations = relations.map((relation) => {
            if (String(relation.to) === String(req.user._id)) return relation.from;
            else return relation.to;
        });

        Activity.findOne({
                _id: req.params._id,
                deleted: false,
                $and: [{
                    $or: [{
                        private: false
                    }, {
                        private: true,
                        $and: [{
                            $or: [{
                                'members.accepted.user': req.user
                            }, {
                                'members.invited.to': req.user
                            }, {
                                user: { $in: relations }
                            }, {
                                user: req.user
                            }]
                        }]
                    }]
                }]
            })
            .select('name date description user tags image level near location members private share')
            .populate('user level tags members.accepted.user members.pending.user members.rejected.user members.invited.to members.invited.from')
            .exec((err, activity) => {
                if (err || !activity) return next(new CustomError(Constants.ERROR_GET_ACTIVITY, 404, err));
                else {
                    let isAccepted = activity.isAccepted(req.user);
                    let isInvited = activity.isInvited(req.user);
                    let isPending = activity.isPending(req.user);

                    activity = activity.toJSON(['level', 'location', 'near', 'members']);
                    activity.user = User.toJSON(activity.user);

                    activity.members.accepted = activity.members.accepted.map((member) => {
                        member.user = User.toJSON(member.user);
                        return member;
                    });

                    if (String(activity.user._id) === String(req.user._id)) {
                        delete activity.near;

                        activity.members.pending = activity.members.pending.map((member) => {
                            member.user = User.toJSON(member.user);
                            return member;
                        });

                        activity.members.accepted = activity.members.accepted.map((member) => {
                            member.user = User.toJSON(member.user);
                            return member;
                        });

                        delete activity.members.invited;
                    } else {
                        if (isAccepted) delete activity.near;
                        else delete activity.location;

                        if (activity.members.accepted.length) {
                            activity.members.accepted = activity.members.accepted.map((member) => {
                                member.user = User.toJSON(member.user);
                                return member;
                            });
                        } else delete activity.members.accepted;

                        if (isInvited) {
                            activity.members.invited = activity.members.invited.filter((member) => {
                                if (String(member.to._id) === String(req.user._id)) {
                                    member.to = User.toJSON(member.to);
                                    member.from = User.toJSON(member.from);
                                    return member;
                                }
                            });
                        } else delete activity.members.invited;

                        if (isPending) {
                            activity.members.pending = activity.members.pending.filter((member) => {
                                if (String(member.user._id) === String(req.user._id)) {
                                    member.user = User.toJSON(member.user);
                                    return member;
                                }
                            });
                        } else delete activity.members.pending;
                    }

                    if (!activity.members.accepted && !activity.members.invited && !activity.members.pending) delete activity.members;
                    else delete activity.members.rejected;

                    activity.tags = activity.tags.map((tag) => {
                        return Tag.toJSON(tag);
                    });

                    let lng = Locale(req.user.locale);
                    activity.level.name = Levels[lng][activity.level.name];

                    return res.status(200).send({ activity: activity });
                }
            });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_ACTIVITY, null, err));
    });
};

exports.getActivities = (req, res, next) => {
    let params = checkParams(req.query, ['lat', 'lng', 'dis', 'skp']);
    if (params) return next(new CustomError(params.message, params.error));

    req.user.getFriends().then((relations) => {
        Tag.getTagsByName(req.query.tags).then((tags) => {
            Level.getLevelByName(req.query.level).then((level) => {
                relations = relations.map((relation) => {
                    if (String(relation.to) === String(req.user._id)) return relation.from;
                    else return relation.to;
                });

                let query = {
                    location: {
                        $near: {
                            $geometry: {
                                type: "Point",
                                coordinates: [req.query.lng, req.query.lat]
                            },
                            $maxDistance: req.query.dis * 1000
                        }
                    },
                    date: {
                        $gte: new Date()
                    },
                    deleted: false,
                    $and: [{
                        $or: [{
                            private: false
                        }, {
                            private: true,
                            $and: [{
                                $or: [{
                                    'members.accepted.user': req.user
                                }, {
                                    'members.invited.to': req.user
                                }, {
                                    user: { $in: relations }
                                }, {
                                    user: req.user
                                }]
                            }]
                        }]
                    }]
                };

                if (tags) {
                    query.tags = {
                        $in: tags
                    };
                }

                if (level) {
                    query.level = level;
                }

                Activity.find(query)
                    .sort({ date: 1 })
                    .skip(req.query.cnt ? 0 : req.query.skp * config.activity.offset)
                    .limit(req.query.cnt ? 0 : config.activity.offset)
                    .select('name description date user image near members private share')
                    .populate('user members.accepted.user members.pending.user members.invited.to members.invited.from')
                    .exec((err, activities) => {
                        if (err || !activities) return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, 404, err));
                        else {
                            activities = activities.map((activity) => {
                                let isInvited = activity.isInvited(req.user);
                                let isPending = activity.isPending(req.user);

                                activity = activity.toJSON(['near', 'members']);
                                activity.user = User.toJSON(activity.user);

                                if (activity.members.accepted.length) {
                                    activity.members.accepted = activity.members.accepted.map((member) => {
                                        member.user = User.toJSON(member.user);
                                        return member;
                                    });
                                } else delete activity.members.accepted;

                                if (isInvited) {
                                    activity.members.invited = activity.members.invited.filter((member) => {
                                        if (String(member.to._id) === String(req.user._id)) {
                                            member.to = User.toJSON(member.to);
                                            member.from = User.toJSON(member.from);
                                            return member;
                                        }
                                    });
                                } else delete activity.members.invited;

                                if (isPending) {
                                    activity.members.pending = activity.members.pending.filter((member) => {
                                        if (String(member.user._id) === String(req.user._id)) {
                                            member.user = User.toJSON(member.user);
                                            return member;
                                        }
                                    });
                                } else delete activity.members.pending;

                                if (!activity.members.accepted && !activity.members.invited && !activity.members.pending) delete activity.members;
                                else delete activity.members.rejected;

                                return activity;
                            });

                            return res.status(200).send({ activities: req.query.cnt ? activities.length : activities });
                        }
                    });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, null, err));
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, null, err));
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, null, err));
    });
};

exports.getMineActivities = (req, res, next) => {
    let params = checkParams(req.query, ['skp']);
    if (params) return next(new CustomError(params.message, params.error));

    let query = {
        date: {
            $gte: new Date()
        },
        deleted: false,
        $and: [{
            $or: [{
                user: req.user
            }, {
                'members.accepted.user': req.user
            }]
        }]
    };

    if (req.query.org) {
        query = {
            date: {
                $gte: new Date()
            },
            deleted: false,
            user: req.user
        };
    }

    if (req.query.pnd) {
        query = {
            date: {
                $gte: new Date()
            },
            deleted: false,
            private: false,
            'members.pending.user': req.user
        };
    }

    Activity.find(query)
        .select('name description date user image location private share')
        .populate('user')
        .skip(req.query.cnt ? 0 : req.query.skp * config.activity.offset)
        .limit(req.query.cnt ? 0 : config.activity.offset)
        .sort({ date: 1 })
        .exec((err, activities) => {
            if (err || !activities) return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, 404, err));
            else {
                Activity.find({
                        date: {
                            $gte: new Date()
                        },
                        deleted: false,
                        'members.invited.to': req.user
                    })
                    .select('name description date user members image near private share')
                    .populate('user members.invited.to members.invited.from')
                    .limit(3)
                    .sort({ date: 1 })
                    .exec((err, invited) => {
                        if (err) return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, null, err));
                        else {
                            invited = invited.map((activity) => {
                                activity = activity.toJSON(['near', 'members']);
                                activity.user = User.toJSON(activity.user);

                                delete activity.members.accepted;
                                delete activity.members.pending;
                                delete activity.members.rejected;

                                activity.members.invited = activity.members.invited.filter((invite) => {
                                    if (String(invite.to._id) === String(req.user._id)) {
                                        invite.to = User.toJSON(invite.to);
                                        invite.from = User.toJSON(invite.from);
                                        return invite;
                                    }
                                });
                                return activity;
                            });
                            activities = activities.map((activity) => {
                                activity = activity.toJSON(['location']);
                                activity.user = User.toJSON(activity.user);
                                return activity;
                            });

                            return res.status(200).send({ invited: invited, activities: activities });
                        }
                    });
            }
        });
};

exports.getHistoryActivities = (req, res, next) => {
    let params = checkParams(req.query, ['skp']);
    if (params) return next(new CustomError(params.message, params.error));

    User.getUserById(req.params._id || req.user._id).then((user) => {
        req.user.getFriends().then((relations) => {
            relations = relations.map((relation) => {
                if (String(relation.to) === String(user._id)) return relation.from;
                else return relation.to;
            });

            let options = [{
                user: req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }]
                }]
            }, {
                user: user,
                $and: [{
                    $or: [{
                        'members.accepted.user': req.user
                    }, {
                        user: { $in: relations }
                    }]
                }]
            }, {
                'members.accepted.user': req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }, {
                        user: { $in: relations },
                        user: user
                    }]
                }]
            }];

            if (String(req.user._id) !== String(user._id)) options.push({ 
                'members.invited.to': req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }, {
                        user: user
                    }]
                }]
            });

            let query = {
                deleted: false,
                $and: [{
                    $or: [{
                        private: false,
                        $and: [{
                            $or: [{
                                'members.accepted.user': user
                            }, {
                                user: user
                            }]
                        }]
                    }, {
                        private: true,
                        $and: [{
                            $or: options
                        }]
                    }]
                }]
            };

            Activity.find(query)
                .sort({ date: 1 })
                .skip(req.query.cnt ? 0 : req.query.skp * config.activity.offset)
                .limit(req.query.cnt ? 0 : config.activity.offset)
                .select('name description date user image near members private share')
                .populate('user members.accepted.user members.pending.user members.invited.to members.invited.from')
                .exec((err, activities) => {
                    if (err || !activities) return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, 404, err));
                    else {
                        activities = activities.map((activity) => {
                            let isInvited = activity.isInvited(req.user);
                            let isPending = activity.isPending(req.user);

                            activity = activity.toJSON(['near', 'members']);
                            activity.user = User.toJSON(activity.user);

                            if (activity.members.accepted.length) {
                                activity.members.accepted = activity.members.accepted.map((member) => {
                                    member.user = User.toJSON(member.user);
                                    return member;
                                });
                            } else delete activity.members.accepted;

                            if (isInvited) {
                                activity.members.invited = activity.members.invited.filter((member) => {
                                    if (String(member.to._id) === String(req.user._id)) {
                                        member.to = User.toJSON(member.to);
                                        member.from = User.toJSON(member.from);
                                        return member;
                                    }
                                });
                            } else delete activity.members.invited;

                            if (isPending) {
                                activity.members.pending = activity.members.pending.filter((member) => {
                                    if (String(member.user._id) === String(req.user._id)) {
                                        member.user = User.toJSON(member.user);
                                        return member;
                                    }
                                });
                            } else delete activity.members.pending;

                            if (!activity.members.accepted && !activity.members.invited && !activity.members.pending) delete activity.members;
                            else delete activity.members.rejected;

                            return activity;
                        });

                        return res.status(200).send({ activities: activities });
                    }
                });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, null, err));
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, null, err));
    });
};

exports.getInvitedActivities = (req, res, next) => {
    let params = checkParams(req.query, ['skp']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.find({
            date: {
                $gte: new Date()
            },
            deleted: false,
            'members.invited.to': req.user
        })
        .skip(req.query.cnt ? 0 : req.query.skp * config.invite.offset)
        .limit(req.query.cnt ? 0 : config.invite.offset)
        .select('name description date user image near members private share')
        .populate('user members.invited.to members.invited.from')
        .sort({ date: 'asc' }).exec((err, activities) => {
            if (err || !activities) return next(new CustomError(Constants.ERROR_GET_ACTIVITIES, 404, err));
            else {
                activities = activities.map((activity) => {
                    activity = activity.toJSON(['near', 'members']);
                    activity.user = User.toJSON(activity.user);

                    delete activity.members.accepted;
                    delete activity.members.pending;
                    delete activity.members.rejected;

                    if (activity.members.invited && activity.members.invited.length) {
                        activity.members.invited = activity.members.invited.map((member) => {
                            member.to = User.toJSON(member.to);
                            member.from = User.toJSON(member.from);
                            return member;
                        });
                    } else {
                        delete activity.members;
                    }

                    return activity;
                });

                return res.status(200).send({ activities: activities });
            }
        });
};

exports.inviteActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findOne({
        _id: req.params._id,
        $and: [{
            $or: [{
                user: req.user
            }, {
                'members.accepted.user': req.user
            }]
        }]
    }).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_INVITE_ACTIVITY, 404, err));

        activity.inviteUsers(req.user, req.body.users).then((activity) => {
            let data = {
                url: config.notifications.endpoint.activity_invite,
                notification: {
                    user: req.user._id,
                    users: req.body.users,
                    activity: activity._id
                }
            };

            notifications.sendPostRequest(data.url, data.notification).then(() => {
                return res.status(200).send({ activity: activity.toJSON() });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_INVITE_ACTIVITY, null, err));
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_INVITE_ACTIVITY, null, err));
        });
    });
};

exports.joinActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_JOIN_ACTIVITY, 404, err));

        activity.joinUser(req.user, req.body.message).then((activity) => {
            let data = {
                url: config.notifications.endpoint.activity_join,
                notification: {
                    user: req.user._id,
                    activity: activity._id
                }
            };

            notifications.sendPostRequest(data.url, data.notification).then(() => {
                return res.status(200).send({ activity: activity.toJSON() });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_JOIN_ACTIVITY, null, err));
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_JOIN_ACTIVITY, null, err));
        });
    });
};

exports.leaveActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_LEAVE_ACTIVITY, 404, err));

        activity.leaveUser(req.user._id).then((activity) => {
            Wall.joinOrLeaveActivity(req.user._id, activity._id).then(() => {
                let data = {
                    url: config.notifications.endpoint.activity_leave,
                    notification: {
                        user: req.user._id,
                        activity: activity._id
                    }
                };

                notifications.sendPostRequest(data.url, data.notification).then(() => {
                    return res.status(200).send({ activity: activity.toJSON() });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
                });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_LEAVE_ACTIVITY, null, err));
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_LEAVE_ACTIVITY, null, err));
        });
    });
};

exports.acceptActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, 404, err));

        if (req.body.user) {
            activity.acceptUser(req.body.user).then((activity) => {
                Wall.joinOrLeaveActivity(req.body.user, activity._id, true).then(() => {
                    let data = {
                        url: config.notifications.endpoint.activity_join_accept,
                        notification: {
                            to: req.body.user,
                            from: activity.user,
                            activity: activity._id
                        }
                    };

                    notifications.sendPostRequest(data.url, data.notification).then(() => {
                        return res.status(200).send({ activity: activity.toJSON() });
                    }).catch((err) => {
                        return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
                    });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
                });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
            });
        } else {
            activity.acceptInvite(req.user._id).then((activity) => {
                Wall.joinOrLeaveActivity(req.user._id, activity._id, true).then(() => {
                    let data = {
                        url: config.notifications.endpoint.activity_invite_accept,
                        notification: {
                            to: req.user._id,
                            from: activity.user,
                            activity: activity._id
                        }
                    };

                    notifications.sendPostRequest(data.url, data.notification).then(() => {
                        return res.status(200).send({ activity: activity.toJSON() });
                    }).catch((err) => {
                        return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
                    });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
                });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_ACCEPT_ACTIVITY, null, err));
            });
        }
    });
};

exports.rejectActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_REJECT_ACTIVITY, 404, err));

        if (req.body.user) {
            activity.rejectUser(req.body.user).then(() => {
                return res.status(200).send({ activity: activity.toJSON() });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_REJECT_ACTIVITY, null, err));
            });
        } else {
            activity.rejectInvite(req.user._id).then((activity) => {
                return res.status(200).send({ activity: activity.toJSON() });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_REJECT_ACTIVITY, null, err));
            });
        }
    });
};

exports.cancelActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_CANCEL_ACTIVITY, 404, err));

        activity.cancelUser(req.user._id).then(() => {
            return res.status(200).send({ activity: activity.toJSON() });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_CANCEL_ACTIVITY, null, err));
        });
    });
};

exports.getPendingActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).populate('members.pending.user').exec((err, activity) => {
        if (err || !activity || String(activity.user) !== String(req.user._id)) return next(new CustomError(Constants.ERROR_PENDING_ACTIVITY, 404, err));

        activity.members.pending = activity.members.pending.map((pending) => {
            pending.user = pending.user.toJSON();
            return pending;
        });

        activity.members.pending = activity.members.pending.sort((a, b) => {
            return b.date - a.date;
        });

        return res.status(200).send({ pending: activity.members.pending });
    });
};

exports.createMessage = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findById(req.params._id).exec((err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_SEND_MESSAGE, 404, err));

        activity.createMessage({ user: req.user, message: req.body.message }).then(() => {
            return res.status(200).send();
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_SEND_MESSAGE, null, err));
        });
    });
};

exports.deleteActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findOneAndUpdate({ _id: req.params._id, user: req.user._id }, { $set: { deleted: true } }, { new: true }, (err, activity) => {
        if (err || !activity) return next(new CustomError(Constants.ERROR_DELETE_ACTIVITY, 404, err));
        else return res.status(200).send({});
    });
};

exports.getActivitiesLocations = (req, res, next) => {
    req.user.getFriends().then((relations) => {
        Tag.getTagsByName(req.query.tags).then((tags) => {
            Level.getLevelByName(req.query.level).then((level) => {
                relations = relations.map((relation) => {
                    if (String(relation.to) === String(req.user._id)) return relation.from;
                    else return relation.to;
                });

                let query = {
                    date: {
                        $gte: new Date()
                    },
                    deleted: false,
                    $and: [{
                        $or: [{
                            private: false
                        }, {
                            private: true,
                            $and: [{
                                $or: [{
                                    'members.accepted.user': req.user
                                }, {
                                    'members.invited.to': req.user
                                }, {
                                    user: { $in: relations }
                                }, {
                                    user: req.user
                                }]
                            }]
                        }]
                    }]
                };

                if (tags) {
                    query.tags = {
                        $in: tags
                    };
                }

                if (level) {
                    query.level = level;
                }

                Activity.find(query).select('near').exec((err, activities) => {
                    if (err || !activities) return next(new CustomError(Constants.ERROR_GET_ACTIVITIES_LOCATIONS, 404, err));
                    else {
                        activities = activities.map((activity) => {
                            return activity.toJSON(['near']);
                        });

                        return res.status(200).send({ activities: activities });
                    }
                });
            });
        });
    });
};

function capitalize(s) {
    return s && s[0].toUpperCase() + s.slice(1);
}
