const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const Tag = require('../../models/Tag');
const Activity = require('../../models/Activity');
const Diacritics = require('../../helpers/diacritics');
const Constants = require('../../data/Constants');
const checkParams = require('../../helpers/check-params');
const waterfall = require('promise-waterfall');

exports.getNearTags = (req, res, next) => {
    let query = {};

    if (req.query.q) query = {
        name: Diacritics(req.query.q)
    };

    Tag.find(query)
        .sort({ activities: -1 })
        .limit(10)
        .exec((err, tags) => {
            if (err) return next(new CustomError(Constants.ERROR_GET_TAGS, null, err));
            else {
                tags = tags.map((tag) => {
                    tag = tag.toJSON(['activities', 'users']);
                    tag.activities = tag.activities.length || 0;
                    tag.users = tag.users.length || 0;;
                    return tag;
                });

                return res.status(200).send({ tags: tags });
            }
        });
};

exports.getHomeTags = (req, res, next) => {
    let params = checkParams(req.query, ['lat', 'lng']);
    if (params) return next(new CustomError(params.message, params.error));

    req.user.getFriends().then((relations) => {
        relations = relations.map((relation) => {
            if (String(relation.to) === String(req.user._id)) return relation.from;
            else return relation.to;
        });

        Activity.getNearPriorityTags(req.user, relations, req.query.lng, req.query.lat).then((near) => {
            Tag.getPriorityTags().then((priority) => {
                if (near.length > 3) near.splice(3, near.length);

                priority = priority.filter((x) => {
                    let tags = near.map((y) => { return String(y._id); });
                    if (tags.indexOf(String(x._id)) === -1) return x;
                    return false;
                });

                priority = priority.map((tag) => {
                    tag = tag.toJSON(['activities']);
                    tag.activities = 0;
                    return tag;
                });

                let trend = [];
                if (near.length) {
                    let promises = near.map((tag) => {
                        return () => {
                            return new Promise((resolve, reject) => {
                                Activity.getActiveActivities(req.user, relations, tag.activities, [req.query.lng, req.query.lat]).then((activities) => {
                                    tag = tag.toJSON(['activities']);
                                    tag.activities = activities.length;
                                    trend.push(tag);
                                    resolve();
                                }).catch((err) => {
                                    reject(err);
                                });
                            });
                        };
                    });

                    waterfall(promises).then(() => {
                        if (trend.length < 3) {
                            for (let i = trend.length; i < 3; i++) {
                                trend.push(priority[0]);
                                priority.shift();
                            }
                        }

                        trend = trend.sort((a, b) => { return b.activities - a.activities; });
                        return res.status(200).send({ trend: trend, recommended: priority });
                    })
                    .catch((err) => {
                        return next(new CustomError(Constants.ERROR_GET_TAGS, null, err));
                    });
                } else {
                    if (trend.length < 3) {
                        for (let i = trend.length; i < 3; i++) {
                            trend.push(priority[0]);
                            priority.shift();
                        }
                    }

                    trend = trend.sort((a, b) => { return b.activities - a.activities; });
                    return res.status(200).send({ trend: trend, recommended: priority });
                }
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_GET_TAGS, null, err));
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_GET_TAGS, null, err));
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_TAGS, null, err));
    });
};
