const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const Log = require('../../models/Log');
const Constants = require('../../data/Constants');

exports.createRequest = function(req, res, next) {
    let ip =  req.headers["X-Forwarded-For"] || req.headers["x-forwarded-for"] || req.client.remoteAddress;
    let user = req.user || null;

    let log = new Log({
        ip: ip,
        user: user,
        method: req.method,
        path: req.originalUrl,
        params: {
            headers: req.headers,
            params: req.params,
            query: req.query,
            body: req.body
        }
    });

    log.save((err) => {
        if (err) return next(new CustomError(Constants.ERROR_CREATE_REQUEST, null, err));
        else next();
    });
};