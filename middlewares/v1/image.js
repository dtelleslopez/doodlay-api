const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const Activity = require('../../models/Activity');
const User = require('../../models/User');
const Image = require('../../models/Image');
const checkParams = require('../../helpers/check-params');
const Constants = require('../../data/Constants');

exports.getImage = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Image.getImage(req.params._id, req.query.sz || 'small').then((image) => {
        return res.set('Content-Type', 'image/jpeg').end(image);
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_IMAGE, null, err));
    });
};

exports.getShareImage = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.findOne({
        image: req.params._id
    }).exec((err, activity) => {
        if (err) return next(new CustomError(Constants.ERROR_GET_IMAGE, null, err));
        else {
            User.findOne({
                image: req.params._id
            }).exec((err, user) => {
                if (err) return next(new CustomError(Constants.ERROR_GET_IMAGE, null, err));
                else if (activity || user) {
                    Image.getImage(req.params._id, req.query.sz || 'small').then((image) => {
                        return res.set('Content-Type', 'image/jpeg').end(image);
                    }).catch((err) => {
                        return next(new CustomError(Constants.ERROR_GET_IMAGE, null, err));
                    });
                } else {
                    return next(new CustomError(Constants.ERROR_GET_IMAGE, null, err));
                }
            });
        }
    });
};

exports.uploadImage = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    let Schema = null;
    let path = req.params._id + '/original/';

    switch (req.params.model) {
        case 'activity':
            path = 'activities/' + path;
            Schema = Activity;
            break;
        case 'user':
            path = 'users/' + path;
            Schema = User;
            break;
        default:
            return next(new CustomError('No valid Schema', 409));
    }

    Image.uploadImage(path, req.file).then((image) => {
        Schema.update({ _id: req.params._id }, { image: image }, (err) => {
            if (err) return next(new CustomError(Constants.ERROR_UPLOAD_IMAGE, null, err));
            return res.status(200).send();
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_UPLOAD_IMAGE, null, err));
    });
};