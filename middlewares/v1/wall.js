const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const User = require('../../models/User');
const Activity = require('../../models/Activity');
const Wall = require('../../models/Wall');
const Constants = require('../../data/Constants');
const checkParams = require('../../helpers/check-params');

exports.getWallUser = (req, res, next) => {
    req.user.getFriends().then((friends) => {
        if (!friends) return next(new CustomError(Constants.ERROR_GET_WALL, 404, err));
        else {
            friends = friends.map((friend) => {
                if (String(req.user._id) === String(friend.to)) return friend.from;
                else return friend.to;
            });

            Wall.getWallByUsersId(friends).then((walls) => {
                let users = [];
                let actions = [];

                walls.map((wall) => {
                    wall = wall.toJSON();

                    wall.actions.map((action) => {
                        action.user = User.toJSON(wall.user);
                        actions.push(action);
                    });
                });

                actions = actions.filter((action) => {
                    if (action.action !== 'leave' && action.action !== 'update') {
                        if(!action.activity.private || (action.activity.private && friends.map((friend) => { return String(friend); }).indexOf(String(action.activity.user)) > -1)) { 
                            if (action.activity.deleted) return;
                            action.activity = Activity.toJSON(action.activity, ['near']);
                            if (users.indexOf(action.activity.user) === -1) users.push(action.activity.user);
                            return action;
                        }
                    }
                });

                actions = actions.sort((a, b) => {
                    return new Date(b.date) - new Date(a.date);
                });

                User.getUsersById(users).then((users) => {
                    actions = actions.map((action) => {
                        users.map((user) => {
                            if (String(user._id) === String(action.activity.user)) action.activity.user = user;
                        });
                        return action;
                    });

                    return res.status(200).send({ wall: actions });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_GET_WALL, null, err));
                });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_GET_WALL, null, err));
            });
        }
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_WALL, null, err));
    });
};

exports.getWallActivity = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    Activity.getActivityById(req.params._id).then((activity) => {
        if (activity.isAccepted(req.user)) {
            Wall.getWallByActivityId(req.params._id).then((wall) => {
                let users = [];

                wall = wall.toJSON();
                wall.actions.map((action) => {
                    if (users.indexOf(action.user) === -1) users.push(action.user);
                });

                User.getUsersById(users).then((users) => {
                    wall.actions = wall.actions.map((action) => {
                        users.map((user) => {
                            if (String(user._id) === String(action.user)) action.user = user.toJSON();
                        });
                        return action;
                    });

                    if (String(wall.activity.user) !== String(req.user._id) || !wall.activity.members.pending.length) {
                        wall.activity = Activity.toJSON(wall.activity, ['near']);
                        return res.status(200).send({ wall: wall });
                    } else {
                        users = [];

                        for (let i = 0; i < wall.activity.members.pending.length; i++) {
                            users.push(wall.activity.members.pending[i].user);
                            if (users.length === 3) break;
                        }

                        User.getUsersById(users).then((users) => {
                            wall.activity.members.pending.map((pending) => {
                                users.map((user) => {
                                    if (String(user._id) === String(pending.user)) pending.user = user.toJSON();
                                });
                                return pending;
                            });

                            let pending = wall.activity.members.pending;
                            pending = pending.sort((a, b) => {
                                return b.date - a.date;
                            });

                            wall.activity = Activity.toJSON(wall.activity, ['near']);

                            return res.status(200).send({ wall: wall, pending: pending});
                        });
                    }
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_GET_WALL, null, err));
                });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_GET_WALL, null, err));
            });
        } else {
            return next(new CustomError(Constants.ERROR_GET_WALL));
        }
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_WALL, null, err));
    });
};