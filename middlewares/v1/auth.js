const config = require('../../config');
const CustomError = require('../../helpers/custom-error');
const jwt = require('jwt-simple');
const User = require('../../models/User');
const Image = require('../../models/Image');
const request = require('request');
const http = require('http');
const checkParams = require('../../helpers/check-params');
const Constants = require('../../data/Constants');
const mailer = require('../../helpers/mailer');
const randomstring = require("randomstring");
const Norris = require('../../helpers/norris');
const Locale = require('../../helpers/locale');
const GoogleAuth = require('google-auth-library');
const Terms = require('../../data/Terms');

exports.ensureAuthenticated = function(req, res, next) {
    let params = checkParams(req.headers, ['authorization']);
    if (params) return next(new CustomError(params.message, params.error));

    let token = req.headers.authorization.split(' ')[1];
    let payload = jwt.decode(token, config.secret);

    if (new Date(payload.exp) <= new Date()) return next(new CustomError(Constants.TOKEN_EXPIRED, 403, null, 'TOKEN_NOT_VALID'));

    User.getUserById(payload.sub).then((user) => {
        req.user = user;
        next();
    }).catch((err) => {
        return next(new CustomError(Constants.TOKEN_NOT_VALID, 401, err, 'TOKEN_NOT_VALID'));
    });
};

exports.signup = (req, res, next) => {
    let params = checkParams(req.body, ['name', 'email', 'password']);
    if (params) return next(new CustomError(params.message, params.error));

    let version = Object.keys(Terms.en);
    version = version[version.length - 1];

    let user = new User({
        name: req.body.name.trim(),
        surname: req.body.surname.trim(),
        email: req.body.email,
        password: req.body.password,
        locale: req.body.locale || config.default.locale,
        region_code: req.body.region_code || config.default.region_code,
        email_token: randomstring.generate(),
        terms: {
            version: version,
            accepted: true
        }
    });

    user = user.addDevice(req.body.device);

    user.createUsername().then((user) => {
        user.save((err, user) => {
            if (err && err.code == 11000) return next(new CustomError(Constants.EMAIL_IN_USE, 403, err, 'EMAIL_ALREADY_IN_USE'));
            else if (err) return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
            else {
                Norris(config.norris.new_user, user);
                mailer.send(user.locale, config.mailer.templates.confirm, { name: user.name, email: user.email, token: user.email_token });
                return res.status(200).send({ user: user.toJSON(['email', 'location', 'sports']), token: user.createToken(user), device: user.getDeviceByToken(req.body.device), first_access: true });
            }
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
    });
};

exports.login = (req, res, next) => {
    let params = checkParams(req.body, ['email', 'password']);
    if (params) return next(new CustomError(params.message, params.error));

    User.findOne({
        email: req.body.email.toLowerCase()
    }, (err, user) => {
        if (err || !user) return next(new CustomError(Constants.NOT_FOUND, 404, err, 'USER_NOT_EXIST'));
        
        if (user.provider) return next(new CustomError(Constants.EMAIL_IN_USE_WITH_PROVIDER + user.provider, 403, null, 'EMAIL_ALREADY_IN_USE_PROVIDER'));
        else if (user.isValidPassword(req.body.password, user)) {
            let response = {};

            if (req.body.region_code) {
                user.region_code = req.body.region_code;
            }

            if (req.body.device && req.body.device.token) {
                user = user.addDevice(req.body.device);
                response = { user: user.toJSON(['email', 'location', 'sports']), token: user.createToken(user), device: user.getDeviceByToken(req.body.device) };
            } else {
                response = { user: user.toJSON(['email', 'location', 'sports']), token: user.createToken(user) };
            }

            user.save((err, user) => {
                return res.status(200).send(response);
            });
        }
        else return next(new CustomError(Constants.INCORRECT_PASSWORD, 403, null, 'INCORRECT_PASSWORD'));
    });
};

exports.social = (req, res, next) => {
    let params = checkParams(req.body, ['social', 'token', 'locale']);
    if (params) return next(new CustomError(params.message, params.error));

    let verifySocial = {
        facebook: verifyFacebook,
        google: verifyGoogle
    };

    verifySocial[req.body.social](req.body.token, req.body.locale, req.body.region_code).then((data) => {
        data.user.save((err, user) => {
            if (err && err.code !== 11000) return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
            else if (err && err.code === 11000) {
                User.findOne({ email: data.user.email }).exec((err, user) => {
                    if (err) return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
                    else if (req.body.device) {
                        user = user.addDevice(req.body.device);
                        user.save((err, user) => {
                            if (err) return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
                            else return res.status(200).send({ user: user.toJSON(['email', 'location', 'sports']), token: user.createToken(user), device: user.getDeviceByToken(req.body.device) });
                        });
                    } else {
                        return res.status(200).send({ user: user.toJSON(['email', 'location', 'sports']), token: user.createToken(user) });
                    }
                });
            } else {
                let version = Object.keys(Terms.en);
                version = version[version.length - 1];

                user.terms = {
                    version: version,
                    accepted: true
                };

                Image.uploadImageURL('users/' + user._id + '/original/', data.image).then((image) => {
                    user.image = image._id;
                    user.createUsername().then((user) => {
                        user = user.addDevice(req.body.device);
                        user.save((err, user) => {
                            if (err && err.code == 11000) return next(new CustomError(Constants.EMAIL_IN_USE, 403, err, 'EMAIL_ALREADY_IN_USE'));
                            else if (err) return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
                            else {
                                Norris(config.norris.new_user, user);
                                mailer.send(user.locale, config.mailer.templates.welcome, { name: user.name, email: user.email });
                                return res.status(200).send({ user: user.toJSON(['email', 'location', 'sports']), token: user.createToken(user), device: user.getDeviceByToken(req.body.device), first_access: true });
                            }
                        });
                    }).catch((err) => {
                        return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
                    });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_CREATE_USER, null, err));
                });
            }
        });
    }).catch((err) => {
        return next(err);
    });
};

function verifyFacebook(token, locale, region_code) {
    return new Promise((resolve, reject) => {
        request(config.facebook.verify_token + token, (err, res, body) => {
            let data = JSON.parse(body);

            if (!err && data.first_name && data.last_name && data.email && data.picture.data.url) {
                let user = new User({
                    name: data.first_name,
                    surname: data.last_name,
                    email: data.email,
                    locale: locale || config.default.locale,
                    region_code: region_code || config.default.region_code,
                    provider: 'facebook'
                });

                resolve({ user: user, image: data.picture.data.url });
            } else {
                reject(err);
            }
        });
    });
};

function verifyGoogle(token, locale, region_code) {
    return new Promise((resolve, reject) => {
        request(config.google.verify_token + token, (err, res, body) => {
            let data = JSON.parse(body);

            if (!err && data.given_name && data.family_name && data.email && data.picture) {
                let user = new User({
                    name: data.given_name,
                    surname: data.family_name,
                    email: data.email,
                    locale: locale || config.default.locale,
                    region_code: region_code || config.default.region_code,
                    provider: 'google'
                });

                resolve({ user: user, image: data.picture + '?sz=500' });
            } else {
                reject(err);
            }
        });
    });
};

function getOAuthClient() {
    return new OAuth2('966078514964-1cf047n70sjei913b0jvi7f6ujgajl3g.apps.googleusercontent.com', '7qsmpJcCnf5JcbvnkV-PD2TE', '');
};