const config = require('../../config');
const mongoose = require('mongoose');
const CustomError = require('../../helpers/custom-error');
const Diacritics = require('../../helpers/diacritics');
const User = require('../../models/User');
const Activity = require('../../models/Activity');
const Support = require('../../models/Support');
const Relation = require('../../models/Relation');
const Tag = require('../../models/Tag');
const Image = require('../../models/Image');
const Location = require('../../models/Location');
const checkParams = require('../../helpers/check-params');
const Constants = require('../../data/Constants');
const Terms = require('../../data/Terms');
const mailer = require('../../helpers/mailer');
const randomstring = require('randomstring');
const Locale = require('../../helpers/locale');

exports.getProfile = (req, res, next) => {
    User.getUserById(req.params._id || req.user._id).then((user) => {
        user.getRelation(req.user._id).then((relation) => {
            let relations = relation && relation.status === config.relation.status.accepted ? [user, req.user] : [req.user]
            
            let options = [{
                user: req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }]
                }]
            }, {
                user: user,
                $and: [{
                    $or: [{
                        'members.accepted.user': req.user
                    }, {
                        user: { $in: relations }
                    }]
                }]
            }, {
                'members.accepted.user': req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }, {
                        user: { $in: relations },
                        user: user
                    }]
                }]
            }];

            if (String(req.user._id) !== String(user._id)) options.push({ 
                'members.invited.to': req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }, {
                        user: user
                    }]
                }]
            });

            let query = {
                deleted: false,
                $and: [{
                    $or: [{
                        private: false,
                        $and: [{
                            $or: [{
                                'members.accepted.user': user
                            }, {
                                user: user
                            }]
                        }]
                    }, {
                        private: true,
                        $and: [{
                            $or: options
                        }]
                    }]
                }]
            };

            Activity.find(query).populate('user').exec((err, activities) => {
                let participating = 0;

                activities.map((activity) => {
                    if (activity.date > new Date()) participating++;
                });

                Tag.getTagsById(user.tags).then((tags) => {
                    if (String(user._id) === String(req.user._id)) user = user.toJSON(['email', 'tags', 'location']);
                    else user = user.toJSON(['tags', 'near']);

                    user.tags = tags.map((tag) => { return tag.toJSON() });

                    if (relation) res.status(200).send({ user: user, participating: participating, activities: activities.length, relation: relation.toJSON() });
                    else res.status(200).send({ user: user, participating: participating, activities: activities.length });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
                });
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
    });
};

exports.getProfileByUsername = (req, res, next) => {
    let params = checkParams(req.params, ['username']);
    if (params) return next(new CustomError(params.message, params.error));

    User.getUserByUsername(req.params.username).then((user) => {
        user.getRelation(req.user._id).then((relation) => {
            let relations = relation && relation.status === config.relation.status.accepted ? [user, req.user] : [req.user]
            
            let options = [{
                user: req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }]
                }]
            }, {
                user: user,
                $and: [{
                    $or: [{
                        'members.accepted.user': req.user
                    }, {
                        user: { $in: relations }
                    }]
                }]
            }, {
                'members.accepted.user': req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }, {
                        user: { $in: relations },
                        user: user
                    }]
                }]
            }, { 
                'members.invited.to': req.user,
                $and: [{
                    $or: [{
                        'members.accepted.user': user
                    }, {
                        user: user
                    }]
                }]
            }];

            let query = {
                deleted: false,
                $and: [{
                    $or: [{
                        private: false,
                        $and: [{
                            $or: [{
                                'members.accepted.user': user
                            }, {
                                user: user
                            }]
                        }]
                    }, {
                        private: true,
                        $and: [{
                            $or: options
                        }]
                    }]
                }]
            };

            Activity.find(query).populate('user').exec((err, activities) => {
                let participating = 0;

                activities.map((activity) => {
                    if (activity.date > new Date()) participating++;
                });

                Tag.getTagsById(user.tags).then((tags) => {
                    if (String(user._id) === String(req.user._id)) user = user.toJSON(['email', 'tags', 'location']);
                    else user = user.toJSON(['tags', 'near']);

                    user.tags = tags.map((tag) => { return tag.toJSON() });

                    if (relation) res.status(200).send({ user: user, participating: participating, activities: activities.length, relation: relation.toJSON() });
                    else res.status(200).send({ user: user, participating: participating, activities: activities.length });
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
                });
            });
        }).catch((err) => {
            return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
        });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
    });
};

exports.searchProfile = (req, res, next) => {
    let participations = [];
    let friends = [req.user._id];
    let regex = Diacritics(req.query.q || '');

    req.user.getFriendsPopulated().then((relations) => {
        relations = relations.filter((relation) => {
            let user = String(relation.to._id) === String(req.user._id) ? relation.from : relation.to;
            if (user.name.match(regex)) return relation;
            else if (user.surname && user.surname.match(regex)) return relation;
        });

        for (var i = relations.length - 1; i >= 0; i--) {
            let participation = {
                relation: relations[i].status
            };

            if (String(relations[i].to._id) === String(req.user._id) && String(relations[i].from._id) === String(req.user._id)) {
                continue;
            } else if (String(relations[i].to._id) === String(req.user._id)) {
                participation.user = relations[i].from;
                friends.push(relations[i].from._id);
            } else {
                participation.user = relations[i].to;
                friends.push(relations[i].to._id);
            }

            participations.push(participation);
        }

        User.find({
                $and: [{
                    _id: {
                        $nin: friends
                    }
                }, {
                    $or: [{
                        name: regex
                    }, {
                        surname: regex
                    }]
                }]
            })
            .sort({name: 1})
            .limit(config.user.offset)
            .exec((err, users) => {
                if (err) return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
                req.user.getRelations(users).then((relations) => {
                    if (relations) participations = participations.concat(relations);
                    if (participations.length) {
                        participations = participations.slice(0, config.user.offset);
                        Activity.getUsersParticipation(req.user, participations).then((participations) => {
                            res.status(200).send({ users: participations });
                        }).catch((err) => {
                            return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
                        });
                    } else {
                        res.status(200).send({ users: participations });
                    }
                }).catch((err) => {
                    return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
                });
            });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_GET_PROFILE, null, err));
    });
};

exports.updateProfile = (req, res, next) => {
    req.user.name = req.body.name || req.user.name;
    req.user.surname = req.body.surname || req.user.surname;
    req.user.username = req.body.username || req.user.username;
    req.user.about = (req.body.about || req.body.about === '') ? req.body.about : req.user.about;
    req.user.location = req.user.location || [];

    if (req.body.latitude && req.body.longitude) req.user.location.unshift({ coordinates: [req.body.longitude, req.body.latitude] });

    Tag.createMultipleUser(req.body.tags, req.user).then((tags) => {
        req.user.tags = (!tags ? req.user.tags : tags);
        if (req.body.image) {
            Image.uploadBase64Image('users/' + req.user._id +  '/original/', req.body.image).then((image) => {
                req.user.image = image;
                req.user.save((err, user) => {
                    if (err) return next(new CustomError(Constants.ERROR_UPDATE_PROFILE, null, err));
                    res.status(200).send({ user: user.toJSON(['email', 'location', 'sports', 'tags']) });
                });
            }).catch((err) => {
                return next(new CustomError(Constants.ERROR_UPDATE_PROFILE, null, err));
            });
        } else {
            req.user.save((err, user) => {
                if (err) return next(new CustomError(Constants.ERROR_UPDATE_PROFILE, null, err));
                res.status(200).send({ user: user.toJSON(['email', 'location', 'sports', 'tags']) });
            });
        }
    });
};

exports.updateDevice = (req, res, next) => {
    let params = checkParams(req.params, ['_id']);
    if (params) return next(new CustomError(params.message, params.error));

    req.user.devices = req.user.devices.map((device) => {
        if (String(device._id) === String(req.params._id)) {
            device.token = req.body.token || device.token;
            device.active = req.body.inactive ? false : true;
        }
        return device;
    });

    req.user.save((err, user) => {
        if (err) return next(new CustomError(Constants.ERROR_UPDATE_DEVICE, null, err));
        res.status(200).send({ device: req.user.getDeviceByToken({ token: req.body.token }) });
    });
};

exports.updateLocation = (req, res, next) => {
    let params = checkParams(req.body, ['latitude', 'longitude']);
    if (params) return next(new CustomError(params.message, params.error));

    Location.addLocation(req.user, [req.body.longitude, req.body.latitude]).then(() => {
        res.status(200).send({ user: req.user.toJSON(['location', 'sports', 'tags']) });
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_UPDATE_LOCATION, null, err));
    });
};

exports.createSupport = (req, res, next) => {
    let params = checkParams(req.body, ['message']);
    if (params) return next(new CustomError(params.message, params.error));

    let support = new Support({
        user: req.user,
        message: req.body.message
    });

    support.save(function(err, support) {
        if (err) return next(new CustomError(Constants.ERROR_SEND_SUPPORT, null, err));
        mailer.send(config.default.locale, config.mailer.templates.support, { name: req.user.name, from: req.user.email, email: config.mailer.support, message: support.message });
        res.status(200).send({});
    });
};

exports.confirmEmail = (req, res, next) => {
    let params = checkParams(req.params, ['email_token']);
    if (params) return next(new CustomError(params.message, params.error));

    let email_token = req.params.email_token;

    User.confirmEmail(email_token).then(() => {
        res.status(200).send({});
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_CONFIRM_EMAIL, null, err));
    });
};

exports.resetPassword = (req, res, next) => {
    if (!req.user.provider) {
        req.user.password_token = randomstring.generate();
        req.user.save((err, user) => {
            if (err) return next(new CustomError(Constants.ERROR_RESET_PASSWORD, null, err));
            mailer.send(req.user.locale, config.mailer.templates.reset, { name: req.user.name, email: req.user.email, token: req.user.password_token });
            res.status(200).send({});
        });
    } else {
        res.status(200).send({});
    }
};

exports.changePassword = (req, res, next) => {
    let params = checkParams(req.body, ['password', 'password_security']);
    if (params) return next(new CustomError(params.mesšage, params.error));

    params = checkParams(req.params, ['password_token']);
    if (params) return next(new CustomError(params.mesšage, params.error));

    User.changePassword(req.body.password, req.body.password_security, req.params.password_token).then(() => {
        res.status(200).send({});
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_CHANGE_PASSWORD, null, err));
    });
};

exports.forgotPassword = (req, res, next) => {
    let params = checkParams(req.body, ['email']);
    if (params) return next(new CustomError(params.mesšage, params.error));

    User.forgotPassword(req.body.email).then((user) => {
        mailer.send(user.locale, config.mailer.templates.reset, { name: user.name, email: user.email, token: user.password_token });
        res.status(200).send({});
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_FORGOT_PASSWORD, null, err));
    });
};

exports.getTerms = (req, res, next) => {
    let lng = Locale(req.user.locale);

    let version = Object.keys(Terms[lng]);
    version = version[version.length - 1];

    let versions = req.user.terms.map((term) => {
        return term.version;
    });

    if (versions.indexOf(version) > -1) res.status(200).send({});
    else res.status(200).send({ version: version, title: Terms[lng][version] });
};

exports.acceptTerms = (req, res, next) => {
    let lng = Locale(req.user.locale);

    let version = Object.keys(Terms[lng]);
    version = version[version.length - 1];

    req.user.acceptTerms(version).then(() => {
        res.status(200).send({});
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_ACCEPT_TERMS, null, err));
    });
};

exports.getShareUser = (req, res, next) => {
    let params = checkParams(req.params, ['share']);
    if (params) return next(new CustomError(params.message, params.error));

    User.findOne({
        username: req.params.share
    }).exec((err, user) => {
        if (err || !user) return next(new CustomError(Constants.ERROR_GET_PROFILE, 404, err));
        else {
            return res.status(200).send({ 
                user: {
                    name: user.surname ? `${user.name} ${user.surname}` : user.name,
                    about: user.about,
                    image: user.image
                }
            });
        }
    });
};

exports.deleteUser = (req, res, next) => {
    User.deleteUser(req.user).then((user) => {
        mailer.send(user.locale, config.mailer.templates.delete, { name: user.name, email: user.email, token: user.delete_token });
        res.status(200).send({});
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_DELETE_USER, null, err));
    });
};

exports.confirmDelete = (req, res, next) => {
    let params = checkParams(req.params, ['deleted_token']);
    if (params) return next(new CustomError(params.message, params.error));

    let deleted_token = req.params.deleted_token;

    User.confirmDelete(deleted_token).then(() => {
        res.status(200).send({});
    }).catch((err) => {
        return next(new CustomError(Constants.ERROR_CONFIRM_DELETE, null, err));
    });
};