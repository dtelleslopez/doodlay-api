const express = require('express');
const auth = require('../middlewares/v1/auth');
const activity = require('../middlewares/v1/activity');
const image = require('../middlewares/v1/image');
const tag = require('../middlewares/v1/tag');
const user = require('../middlewares/v1/user');
const relation = require('../middlewares/v1/relation');
const wall = require('../middlewares/v1/wall');
const log = require('../middlewares/v1/log');
const multer = require('multer');
const upload = multer({ dest: './tmp' });
const router = express.Router();

router.get('/', (req, res, next) => { 
	if (req.accepts('html')) {
        res.render('404.html');
    }
    else if (req.accepts('json')) {
        res.status(404).send({ error: 'Not found' });
    }
    else {
        res.status(404).type('txt').send('Not found');
    }
});

router.post('/signup', log.createRequest, auth.signup);
router.post('/login', log.createRequest, auth.login);
router.post('/social', log.createRequest, auth.social);
router.get('/levels', auth.ensureAuthenticated, log.createRequest, activity.getLevels);
router.post('/activity', auth.ensureAuthenticated, log.createRequest, activity.createActivity);
router.put('/activity/:_id', auth.ensureAuthenticated, log.createRequest, activity.updateActivity);
router.get('/activity/:_id', auth.ensureAuthenticated, log.createRequest, activity.getActivity);
router.post('/activity/:_id/invite', auth.ensureAuthenticated, log.createRequest, activity.inviteActivity);
router.post('/activity/:_id/join', auth.ensureAuthenticated, log.createRequest, activity.joinActivity);
router.post('/activity/:_id/leave', auth.ensureAuthenticated, log.createRequest, activity.leaveActivity);
router.post('/activity/:_id/accept', auth.ensureAuthenticated, log.createRequest, activity.acceptActivity);
router.post('/activity/:_id/reject', auth.ensureAuthenticated, log.createRequest, activity.rejectActivity);
router.post('/activity/:_id/cancel', auth.ensureAuthenticated, log.createRequest, activity.cancelActivity);
router.post('/activity/:_id/message', auth.ensureAuthenticated, log.createRequest, activity.createMessage);
router.get('/activity/:_id/wall', auth.ensureAuthenticated, log.createRequest, wall.getWallActivity);
router.get('/activity/:_id/pending', auth.ensureAuthenticated, log.createRequest, activity.getPendingActivity);
router.delete('/activity/:_id', auth.ensureAuthenticated, log.createRequest, activity.deleteActivity);
router.get('/activity/share/:share', auth.ensureAuthenticated, log.createRequest, activity.getActivityByShare);
router.get('/activities', auth.ensureAuthenticated, log.createRequest, activity.getActivities);
router.get('/activities/mine', auth.ensureAuthenticated, log.createRequest, activity.getMineActivities);
router.get('/activities/invited', auth.ensureAuthenticated, log.createRequest, activity.getInvitedActivities);
router.get('/activities/history/:_id?', auth.ensureAuthenticated, log.createRequest, activity.getHistoryActivities);
router.get('/activities/locations', auth.ensureAuthenticated, log.createRequest, activity.getActivitiesLocations);
router.get('/image/:_id', auth.ensureAuthenticated, log.createRequest, image.getImage);
router.get('/image/share/:_id', log.createRequest, image.getShareImage);
//router.post('/image/:model/:_id', auth.ensureAuthenticated, upload.single('image'), image.uploadImage);
router.get('/tags', auth.ensureAuthenticated, log.createRequest, tag.getNearTags);
router.get('/tags/home', auth.ensureAuthenticated, log.createRequest, tag.getHomeTags);
router.post('/profile/forgot', log.createRequest, user.forgotPassword);
router.get('/profile/terms/accept', auth.ensureAuthenticated, log.createRequest, user.acceptTerms);
router.get('/profile/confirm/:email_token', log.createRequest, user.confirmEmail);
router.put('/profile/reset/:password_token', log.createRequest, user.changePassword);
router.put('/profile/device/:_id', auth.ensureAuthenticated, log.createRequest, user.updateDevice);
router.get('/profile/username/:username', auth.ensureAuthenticated, log.createRequest, user.getProfileByUsername);
router.get('/profile/delete/:deleted_token', log.createRequest, user.confirmDelete);
router.get('/profile/search', auth.ensureAuthenticated, log.createRequest, user.searchProfile);
router.get('/profile/reset', auth.ensureAuthenticated, log.createRequest, user.resetPassword);
router.post('/profile/support', auth.ensureAuthenticated, log.createRequest, user.createSupport);
router.put('/profile/location', auth.ensureAuthenticated, log.createRequest, user.updateLocation);
router.get('/profile/terms', auth.ensureAuthenticated, log.createRequest, user.getTerms);
router.get('/profile/:_id?', auth.ensureAuthenticated, log.createRequest, user.getProfile);
router.put('/profile', auth.ensureAuthenticated, log.createRequest, user.updateProfile);
router.delete('/profile', auth.ensureAuthenticated, log.createRequest, user.deleteUser);
router.get('/relation/:_id/join', auth.ensureAuthenticated, log.createRequest, relation.joinFriend);
router.get('/relation/:_id/accept', auth.ensureAuthenticated, log.createRequest, relation.acceptFriend);
router.get('/relation/:_id/cancel', auth.ensureAuthenticated, log.createRequest, relation.cancelFriend);
router.get('/relation/:_id/reject', auth.ensureAuthenticated, log.createRequest, relation.rejectFriend);
router.delete('/relation/:_id', auth.ensureAuthenticated, log.createRequest, relation.deleteFriend);
router.get('/wall', auth.ensureAuthenticated, log.createRequest, wall.getWallUser);
router.get('/e/:share', log.createRequest, activity.getShareActivity);
router.get('/u/:share', log.createRequest, user.getShareUser);

module.exports = router;