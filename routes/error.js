const express = require('express');
const router = express.Router();
const fs = require('fs');

router.get('*', (req, res, next) => { 
	if (req.accepts('html')) {
        res.render('404.html');
    }
    else if (req.accepts('json')) {
        res.status(404).send({ error: 'Not found' });
    }
    else {
        res.status(404).type('txt').send('Not found');
    }
});

module.exports = router;