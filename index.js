const config = require('./config');
const routes = require('./routes');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const express = require('express');
const http = require('http');
const morgan = require('morgan');
const RateLimit = require('express-rate-limit');
const helmet = require('helmet');
const cors = require('cors');
const mongoSanitize = require('express-mongo-sanitize');
const path = require('path');
const Norris = require('./helpers/norris');
const app = express();

mongoose.Promise = require('bluebird');

app.enable('trust proxy');

const limiter = new RateLimit({
	windowMs: config.limiter.windowMs,
	max: config.limiter.max,
	delayMs: config.limiter.delayMs
});

app.engine('.html', require('ejs').__express);
app.use('/statics', express.static(path.join(__dirname, 'views')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
 
//app.use(limiter);
app.use(morgan('dev'));
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }));
app.use(mongoSanitize());
app.use(helmet());
app.use(cors());
app.use('/v1', routes.v1);
app.use('/', routes.v1);
app.use('*', routes.error);
app.use((err, req, res, next) => {
    res.status(err.status || 500).send(err);
});

mongoose.connect(config.mongo, (err) => {
    //if (err) throw err;
    http.createServer(app).listen(config.port);
    Norris(config.norris.start_server);
    console.log('Magic happens on port ' + config.port);
});