module.exports = {
    mongo: 'mongodb://localhost:27017/doodlay',
    secret: 'HobbitstoIsengard',
    port: process.env.port || 3000,
    default: {
        locale: 'en_US',
        region_code: 'ES'
    },
    limiter: {
        windowMs: 15*60*1000,
        max: 300,
        delayMs: 0
    },
    aws_acces_key: 'AKIAIB4QQJDDZ2THLHOA',
    aws_secret_key: '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',
    aws_region: 'eu-west-1',
    aws_s3: 'https://s3.eu-central-1.amazonaws.com/doodlay-images/',
    aws_buket_images: 'doodlay-images',
    mailer: {
        host: 'smtp.doodlay.co',
        port: 587,
        secure: false,
        user: 'hello@doodlay.co',
        pass: '71*Ddly*freeland*02',
        support: 'support@doodlay.co',
        templates: {
            confirm: 'confirm',
            reset: 'reset',
            welcome: 'welcome',
            support: 'support',
            delete: 'delete'
        }
    },
    norris: {
        start_server: 0,
        create_activity: 1,
        new_user: 2
    },
    web: {
        endpoint: 'https://doodlay.co/'
    },
    facebook: {
        verify_token: 'https://graph.facebook.com/me?fields=email,first_name,last_name,picture.width(500).height(500)&access_token='
    },
    google: {
        verify_token: 'https://www.googleapis.com/oauth2/v3/userinfo?access_token='
    },
    activity: {
        offset: 6
    },
    invite: {
        offset: 10
    },
    user: {
        offset: 10
    },
    tag: {
        offset: 10,
        distance: 50000
    },
    relation: {
        status: {
            pending: 'pending',
            accepted: 'accepted',
            canceled: 'canceled',
            rejected: 'rejected',
            deleted: 'deleted',
            chat: 'chat'
        }
    },
    wall: {
        actions: {
            create: 'create',
            update: 'update',
            join: 'join',
            leave: 'leave'
        }
    },
    notifications: {
        host: 'http://localhost:1111/notifications/v1/',
        endpoint: {
            user: 'user',
            activity: 'activity',
            user_join: 'user/join',
            user_accept: 'user/accept',
            activity_join: 'activity/join',
            activity_invite: 'activity/invite',
            activity_join_accept: 'activity/join/accept',
            activity_invite_accept: 'activity/invite/accept',
            activity_leave: 'activity/leave'
        }
    } 
};
