const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SupportSchema = new Schema({
    user: { type: Schema.ObjectId, ref: 'User' },
    date: { type: Date, default: Date.now },
    message: String
});

mongoose.model('Support', SupportSchema);
const Support = mongoose.model('Support');
module.exports = Support;