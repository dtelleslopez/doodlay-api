const config = require('../config');
const mongoose = require('mongoose');
const waterfall = require('promise-waterfall');
const Schema = mongoose.Schema;

const TagSchema = new Schema({
    name: String,
    image: { type: Schema.ObjectId, ref: 'Image' },
    activities: [{ type: Schema.ObjectId, ref: 'Activity' }],
    users: [{ type: Schema.ObjectId, ref: 'User' }],
    priority: Boolean,
    date: { type: Date, default: Date.now }
});

TagSchema.methods.toJSON = function(properties) {
    let tag = this.toObject();
    let basics = ['_id', 'name', 'image', 'priority'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(tag).forEach((key) => {
        if (basics.indexOf(key) === -1) delete tag[key];
    });

    return tag;
};

TagSchema.statics.toJSON = function(tag, properties) {
    let basics = ['_id', 'name', 'image', 'priority'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(tag).forEach((key) => {
        if (basics.indexOf(key) === -1) delete tag[key];
    });

    return tag;
};

TagSchema.statics.createMultipleActivity = (names, activity) => {
    return new Promise((resolve, reject) => {
        let tags = [];
        if (!names || !names.length) resolve(tags);

        let promises = names.map((name) => {
            return () => {
                return new Promise((resolve, reject) => {
                    Tag.findOneAndUpdate({
                        name: name.trim().toLowerCase()
                    }, {
                        $addToSet: {
                            activities: activity
                        }
                    }, {
                        upsert: true,
                        new: true,
                        setDefaultsOnInsert: true
                    }, (err, tag) => {
                        if (err) return reject(err);
                        else {
                            tags.push(tag.toJSON());
                            return resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                resolve(tags);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

TagSchema.statics.createMultipleUser = (names, user) => {
    return new Promise((resolve, reject) => {
        if (Number(names) === -1) return resolve([]); 
        if (!names || !names.length) return resolve();        

        let tags = [];

        let promises = names.map((name) => {
            return () => {
                return new Promise((resolve, reject) => {
                    Tag.findOneAndUpdate({
                        name: name.trim().toLowerCase()
                    }, {
                        $addToSet: {
                            users: user
                        }
                    }, {
                        upsert: true,
                        new: true,
                        setDefaultsOnInsert: true
                    }, (err, tag) => {
                        if (err) return reject(err);
                        else {
                            tags.push(tag);
                            return resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                resolve(tags);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

TagSchema.statics.getTagsById = (tags) => {
    return new Promise((resolve, reject) => {
        if (!tags) return resolve([]);
        else {
            Tag.find({ _id: { $in: tags } }).exec((err, tags) => {
                if (err) return reject(err);
                else return resolve(tags);
            });
        }
    });
};

TagSchema.statics.getTagsByName = (tags) => {
    return new Promise((resolve, reject) => {
        if (!tags) return resolve();
        else {
            tags = tags.toLocaleString().toLowerCase().split(',');

            Tag.find({ name: { $in: tags } }).exec((err, tags) => {
                if (err) return reject(err);
                else return resolve(tags);
            });
        }
    });
};

TagSchema.statics.getPriorityTags = () => {
    return new Promise((resolve, reject) => {
        Tag.find({ priority: true }).sort({ activities: -1 }).limit(config.tag.offset).exec((err, tags) => {
            if (err) reject(err);
            else resolve(tags);
        });
    });
};

mongoose.model('Tag', TagSchema);
const Tag = mongoose.model('Tag');
module.exports = Tag;