const mongoose = require('mongoose');
const waterfall = require('promise-waterfall');
const nearLocation = require('../helpers/near-location');
const randomstring = require('randomstring');
const config = require('../config');
const Schema = mongoose.Schema;

const Member = {
    user: { type: Schema.ObjectId, ref: 'User' },
    to: { type: Schema.ObjectId, ref: 'User' },
    from: { type: Schema.ObjectId, ref: 'User' },
    date: { type: Date, default: Date.now },
    message: String
};

const Message = {
    user: { type: Schema.ObjectId, ref: 'User' },
    date: { type: Date, default: Date.now },
    message: String,
    readed: [{
        type: Schema.ObjectId,
        ref: "User"
    }]
};

const ActivitySchema = new Schema({
    name: String,
    description: String,
    share: String,
    date: Date,
    level: { type: Schema.ObjectId, ref: 'Level' },
    private: { type: Boolean, default: false },
    image: { type: Schema.ObjectId, ref: 'Image' },
    user: { type: Schema.ObjectId, ref: 'User' },
    creation: { type: Date, default: Date.now },
    near: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    },
    location: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    },
    deleted: { type: Boolean, default: false },
    messages: [Message],
    tags: [{ type: Schema.ObjectId, ref: 'Tag' }],
    members: {
        invited: [Member],
        pending: [Member],
        accepted: [Member],
        rejected: [Member]
    }
});

ActivitySchema.index({ location: '2dsphere' });

ActivitySchema.pre('save', function(next) {
    let activity = this;

    if (activity.isModified('location')) activity.near.coordinates = nearLocation(activity.location.coordinates, 500);

    if (activity.isNew) {
        Activity
            .find()
            .select('share')
            .exec((err, activities) => {
                activities = activities.map((activity) => {
                    return activity.share;
                });

                let share = randomstring.generate(8);
                while (activities.indexOf(share) > -1) {
                    share = randomstring.generate(8);
                }

                activity.share = share;

                return next();
            });
    } else {
        return next();
    }
});

ActivitySchema.methods.toJSON = function(properties) {
    let activity = this.toObject();
    let basics = ['_id', 'name', 'description', 'share', 'date', 'level', 'image', 'user', 'tags', 'private'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(activity).forEach((key) => {
        if (basics.indexOf(key) === -1) delete activity[key];
    });

    return activity;
};

ActivitySchema.methods.isPending = function(user) {
    return this.members.pending.some(function(member) {
        return member.user.equals(user._id);
    });
};

ActivitySchema.methods.isInvited = function(user) {
    return this.members.invited.some(function(member) {
        return member.to.equals(user._id);
    });
};

ActivitySchema.methods.isAccepted = function(user) {
    return this.members.accepted.some(function(member) {
        return member.user.equals(user._id);
    });
};

ActivitySchema.methods.isRejected = function(user) {
    return this.members.rejected.some(function(member) {
        return member.user.equals(user._id);
    });
};

ActivitySchema.methods.getMembers = function() {
    let members = {
        accepted: this.members.accepted.map((member) => { return String(member.user) }),
        pending: this.members.pending.map((member) => { return String(member.user) }),
        rejected: this.members.rejected.map((member) => { return String(member.user) }),
        invited: this.members.invited.map((member) => { return String(member.to) })
    };

    return members;
};

ActivitySchema.methods.inviteUsers = function(member, users) {
    let activity = this;

    return new Promise((resolve, reject) => {
        isInvited = false;

        users.map((user) => {
            if (!activity.isAccepted(user) && !activity.isPending(user) && !activity.isInvited(user)) {
                if (activity.isRejected(user)) {
                    activity.members.rejected = activity.members.rejected.filter(function(member) {
                        if (!member.user.equals(user._id)) return member;
                    });
                }

                activity.members.invited.push({ to: user, from: member });
                isInvited = true;
            }
        });

        if (isInvited) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.joinUser = function(user, message) {
    let activity = this;

    return new Promise((resolve, reject) => {
        if (activity.isAccepted(user) || activity.isPending(user) || activity.isInvited(user)) {
            reject();
        } else {
            if (activity.isRejected(user)) {
                activity.members.rejected = activity.members.rejected.filter(function(member) {
                    if (!member.user.equals(user._id)) return member;
                });
            }

            activity.members.pending.push({
                user: user,
                message: message
            });

            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        }
    });
};

ActivitySchema.methods.leaveUser = function(_id) {
    let activity = this;

    return new Promise((resolve, reject) => {
        let isLeaving = false;

        activity.members.accepted = activity.members.accepted.filter(function(member) {
            if (!member.user.equals(_id)) return member;
            else isLeaving = true;
        });

        if (isLeaving) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.acceptUser = function(_id) {
    let activity = this;

    return new Promise((resolve, reject) => {
        let isAccepted = false;

        activity.members.pending = activity.members.pending.filter(function(member) {
            if (member.user.equals(_id)) {
                activity.members.accepted.push(member);
                isAccepted = true;
            } else return member;
        });

        if (isAccepted) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.acceptInvite = function(_id) {
    let activity = this;

    return new Promise((resolve, reject) => {
        let isAccepted = false;

        activity.members.invited = activity.members.invited.filter(function(member) {
            if (member.to.equals(_id)) {
                activity.members.accepted.push({ user: member.to });
                isAccepted = true;
            } else return member;
        });

        if (isAccepted) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.rejectUser = function(_id) {
    let activity = this;

    return new Promise((resolve, reject) => {
        let isRejected = false;

        activity.members.pending = activity.members.pending.filter(function(member) {
            if (member.user.equals(_id)) {
                activity.members.rejected.push(member);
                isRejected = true;
            } else return member;
        });

        if (isRejected) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.rejectInvite = function(_id) {
    let activity = this;

    return new Promise((resolve, reject) => {
        let isRejected = false;

        activity.members.invited = activity.members.invited.filter(function(member) {
            if (member.to.equals(_id)) {
                activity.members.rejected.push(member);
                isRejected = true;
            } else return member;
        });

        if (isRejected) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.cancelUser = function(_id) {
    let activity = this;

    return new Promise((resolve, reject) => {
        let isCanceled = false;

        activity.members.pending = activity.members.pending.filter(function(member) {
            if (!member.user.equals(_id)) return member;
            else isCanceled = true;
        });

        if (isCanceled) {
            activity.save((err, activity) => {
                if (err) reject(err);
                else resolve(activity);
            });
        } else {
            reject();
        }
    });
};

ActivitySchema.methods.createMessage = function(message) {
    let activity = this;

    return new Promise((resolve, reject) => {
        if (activity.isAccepted(message.user)) {
            message.user = message.user._id;
            activity.messages.push(message);
        }

        activity.save((err, activity) => {
            if (err) reject(err);
            else resolve(activity);
        });
    });
};

ActivitySchema.statics.toJSON = function(activity, properties) {
    let basics = ['_id', 'name', 'description', 'share', 'date', 'level', 'image', 'user', 'tags'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(activity).forEach((key) => {
        if (basics.indexOf(key) === -1) delete activity[key];
    });

    return activity;
};

ActivitySchema.statics.isInMembers = (members, user) => {
    user = String(user);

    return (members.accepted.indexOf(user) > -1 || members.pending.indexOf(user) > -1 || members.rejected.indexOf(user) > -1 || members.invited.indexOf(user) > -1);
};

ActivitySchema.statics.getActivityById = (_id) => {
    return new Promise((resolve, reject) => {
        Activity.findOne({ _id: _id }).exec((err, activity) => {
            if (err || !activity) return reject(err);
            return resolve(activity);
        });
    });
};

ActivitySchema.statics.getUsersParticipation = (user, participations) => {
    return new Promise((resolve, reject) => {
        let promises = participations.map((participation) => {
            return () => {
                return new Promise((resolve, reject) => {
                    let relations = participation.relation === config.relation.status.accepted ? [participation.user, user] : [user]

                    let options = [{
                        user: user,
                        $and: [{
                            $or: [{
                                'members.accepted.user': participation.user
                            }]
                        }]
                    }, {
                        user: participation.user,
                        $and: [{
                            $or: [{
                                'members.accepted.user': user
                            }, {
                                user: { $in: relations }
                            }]
                        }]
                    }, {
                        'members.accepted.user': user,
                        $and: [{
                            $or: [{
                                'members.accepted.user': participation.user
                            }, {
                                user: { $in: relations },
                                user: participation.user
                            }]
                        }]
                    }];

                    if (String(user._id) !== String(participation.user._id)) options.push({
                        'members.invited.to': user,
                        $and: [{
                            $or: [{
                                'members.accepted.user': participation.user
                            }, {
                                user: participation.user
                            }]
                        }]
                    });

                    let query = {
                        deleted: false,
                        $and: [{
                            $or: [{
                                private: false,
                                $and: [{
                                    $or: [{
                                        'members.accepted.user': participation.user
                                    }, {
                                        user: participation.user
                                    }]
                                }]
                            }, {
                                private: true,
                                $and: [{
                                    $or: options
                                }]
                            }]
                        }]
                    };

                    Activity.find(query).exec((err, activities) => {
                        if (err) reject(err);
                        else {
                            let participating = 0;

                            activities.map((activity) => {
                                if (activity.date > new Date()) participating++;
                            });

                            participation.participating = participating;
                            participation.activities = activities.length;

                            resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                resolve(participations);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

ActivitySchema.statics.getNearPriorityTags = (user, relations, lng, lat) => {
    return new Promise((resolve, reject) => {
        let today = new Date();

        Activity.find({
                location: {
                    $near: {
                        $geometry: {
                            type: "Point",
                            coordinates: [lng, lat]
                        },
                        $maxDistance: config.tag.distance
                    }
                },
                date: {
                    $gte: new Date(),
                    $lt: new Date(today.getFullYear(), today.getMonth() + 1, today.getDate())
                },
                deleted: false,
                $and: [{
                    $or: [{
                        private: false
                    }, {
                        private: true,
                        $and: [{
                            $or: [{
                                'members.accepted.user': user
                            }, {
                                'members.invited.to': user
                            }, {
                                user: { $in: relations }
                            }, {
                                user: user
                            }]
                        }]
                    }]
                }]
            })
            .select('tags')
            .populate('tags')
            .exec((err, activities) => {
                if (err) reject(err);
                else {
                    let tags = [];

                    for (let i = 0; i < activities.length; i++) {
                        if (tags.length >= 10) break;
                        for (let j = 0; j < activities[i].tags.length; j++) {
                            if (tags.length >= 10) break;
                            if (activities[i].tags[j].priority) tags.push(activities[i].tags[j]);
                        }
                    };

                    tags = tags.sort((a, b) => {
                        return b.activities.length - a.activities.length;
                    });

                    resolve(tags);
                }
            });
    });
};

ActivitySchema.statics.getActiveActivities = (user, relations, activities, coordinates) => {
    return new Promise((resolve, reject) => {
        Activity.find({
            _id: {
                $in: activities
            },
            location: {
                $near: {
                    $geometry: {
                        type: "Point",
                        coordinates: coordinates
                    },
                    $maxDistance: config.tag.distance
                }
            },
            date: {
                $gte: new Date()
            },
            deleted: false,
            $and: [{
                $or: [{
                    private: false
                }, {
                    private: true,
                    $and: [{
                        $or: [{
                            'members.accepted.user': user
                        }, {
                            'members.invited.to': user
                        }, {
                            user: { $in: relations }
                        }, {
                            user: user
                        }]
                    }]
                }]
            }]
        }).exec((err, activities) => {
            if (err) return reject(err);
            return resolve(activities);
        });
    });
};

mongoose.model('Activity', ActivitySchema);
const Activity = mongoose.model('Activity');
module.exports = Activity;