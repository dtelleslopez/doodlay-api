const config = require('../config');
const mongoose = require('mongoose');
const CustomError = require('../helpers/custom-error');
const Schema = mongoose.Schema;
const AWS = require('aws-sdk');
const fs = require('fs');
const request = require('request');

AWS.config.update({ accessKeyId: config.aws_acces_key, secretAccessKey: config.aws_secret_key });
AWS.config.update({ region: config.aws_region });

const envBucket = (process.env.NODE_ENV  ? process.env.NODE_ENV : 'development') + '/';
const s3Bucket = new AWS.S3({ params: { Bucket: config.aws_buket_images } });
const ImageSchema = new Schema({
    url: String
});

ImageSchema.statics.uploadImage = (path, file) => {
    return new Promise((resolve, reject) => {
        if (!path) reject(new CustomError('Path not provided', 409));
        if (!file || file.mimetype !== 'image/jpeg' || file.size > 5242880) reject(new CustomError('No valid file format or size', 409));

        fs.readFile(file.path, function(err, image) {
            let body = new Buffer(image, 'binary');

            let params = {
                Key: envBucket + path + new Date().getTime() + '.jpg',
                Body: body,
                ContentType: file.mimetype
            };

            s3Bucket.putObject(params, (err, data) => {
                if (err) reject(err);
                let image = new Image({ url: params.Key });
                image.save((err, image) => {
                    if (err) reject(err);
                    else resolve(image);
                });
            });
        });
    });
};

ImageSchema.statics.uploadBase64Image = (path, file) => {
    return new Promise((resolve, reject) => {
        if (!path) reject(new CustomError('Path not provided', 409));
        if (!file) resolve();

        let body = new Buffer(file.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        let params = {
            Key: envBucket + path + new Date().getTime() + '.jpg',
            Body: body,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg'
        };

        s3Bucket.putObject(params, (err, data) => {
            if (err) reject(err);
            let image = new Image({ url: params.Key });
            image.save((err, image) => {
                if (err) reject(err);
                else resolve(image);
            });
        });
    });
};

ImageSchema.statics.uploadImageURL = (path, url) => {
    return new Promise((resolve, reject) => {
        if (!path) reject(new CustomError('Path not provided', 409));
        if (!url) reject(new CustomError('url not provided', 409));

        request({
            url: url,
            encoding: null
        }, function(err, res, body) {
            if (err) reject(err);

            let mimetype = res.headers['content-type'];

            if (mimetype === 'image/jpg' || mimetype === 'image/jpeg' || mimetype === 'image/png') {
                path = envBucket + path + new Date().getTime() + '.jpg';
                
                let params = { 
                    Key: path, 
                    Body: body, 
                    ContentType: mimetype 
                };

                s3Bucket.putObject(params, (err, data) => {
                    if (err) reject(err);
                    let image = new Image({ url: params.Key });
                    image.save((err, image) => {
                        if (err) reject(err);
                        else resolve(image);
                    });
                });
            } else {
                reject(new CustomError('No valid file format', 409));
            }
        });
    });
};

ImageSchema.statics.getImage = (_id, size) => {
    return new Promise((resolve, reject) => {
        Image.findById(_id).exec((err, image) => {
            if (err) reject(err);
            else {
                let resize = image.url;

                s3Bucket.getObject({ Key: resize.replace('original', size) }, function(err, file) {
                    if (err && err.code === 'NoSuchKey' && !file) {
                        s3Bucket.getObject({ Key: image.url }, function(err, file) {
                            if (err || !file) reject(new CustomError('File not found', 404));
                            else resolve(file.Body);
                        });
                    } else if (err && err.code !== 'NoSuchKey') {
                        reject(new CustomError('File not found', 404));
                    } else {
                        resolve(file.Body);
                    }
                });
            }
        });
    });
};

mongoose.model('Image', ImageSchema);
const Image = mongoose.model('Image');
module.exports = Image;