const config = require('../config');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Action = {
    date: { type: Date, default: Date.now },
    user: { type: Schema.ObjectId, ref: 'User' },
    activity: { type: Schema.ObjectId, ref: 'Activity' },
    relation: { type: Schema.ObjectId, ref: 'Relation' },
    action: String
};

const WallSchema = new Schema({
    user: { type: Schema.ObjectId, ref: 'User' },
    activity: { type: Schema.ObjectId, ref: 'Activity' },
    actions: [Action]
});

WallSchema.methods.toJSON = function(properties) {
    let wall = this.toObject();
    let basics = ['_id', 'user', 'activity', 'actions'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(wall).forEach((key) => {
        if (basics.indexOf(key) === -1) delete wall[key];
    });

    return wall;
};

WallSchema.statics.getWallByUserId = (_id) => {
    return new Promise((resolve, reject) => {
        Wall.findOne({
            user: _id
        }).exec((err, wall) => {
            if (err) return reject(err);
            else if (!wall) {
                let wall = new Wall({ user: _id });

                wall.save((err, wall) => {
                    if (err) return reject(err);
                    else return resolve(wall);
                });
            } else {
                return resolve(wall);
            }
        });
    });
};

WallSchema.statics.getWallByActivityId = (_id) => {
    return new Promise((resolve, reject) => {
        Wall.findOne({
            activity: _id
        })
        .populate('activity')
        .exec((err, wall) => {
            if (err) return reject(err);
            else if (!wall) {
                let wall = new Wall({ activity: _id });

                wall.save((err, wall) => {
                    if (err) return reject(err);
                    else return resolve(wall);
                });
            } else {
                return resolve(wall);
            }
        });
    });
};

WallSchema.statics.createOrUpdateActivity = (activity, isNew) => {
    return new Promise((resolve, reject) => {
        Wall.getWallByUserId(activity.user).then((wall) => {
            wall.actions.unshift({
                activity: activity._id,
                action: isNew ? config.wall.actions.create : config.wall.actions.update
            });

            wall.save((err) => {
                if (err) return reject(err);
                else {
                    Wall.getWallByActivityId(activity._id).then((wall) => {
                        wall.actions.unshift({
                            user: activity.user,
                            action: isNew ? config.wall.actions.create : config.wall.actions.update
                        });

                        wall.save((err) => {
                            if (err) return reject(err);
                            else return resolve();
                        });
                    }).catch((err) => {
                        return reject(err);
                    });
                }
            });
        }).catch((err) => {
            return reject(err);
        });
    });
};

WallSchema.statics.joinOrLeaveActivity = (user, activity, isJoining) => {
    return new Promise((resolve, reject) => {
        Wall.getWallByUserId(user).then((wall) => {
            wall.actions.unshift({
                activity: activity,
                action: isJoining ? config.wall.actions.join : config.wall.actions.leave
            });

            wall.save((err) => {
                if (err) return reject(err);
                else {
                    Wall.getWallByActivityId(activity).then((wall) => {
                        wall.actions.unshift({
                            user: user,
                            action: isJoining ? config.wall.actions.join : config.wall.actions.leave
                        });

                        wall.save((err) => {
                            if (err) return reject(err);
                            else return resolve();
                        });
                    }).catch((err) => {
                        return reject(err);
                    });
                }
            });
        }).catch((err) => {
            return reject(err);
        });
    });
};

WallSchema.statics.getWallByUsersId = (users) => {
    return new Promise((resolve, reject) => {
        Wall.find({
            user: {
                $in: users
            }
        })
        .populate('user actions.activity')
        .exec((err, walls) => {
            if (err) return reject(err);
            else return resolve(walls);
        });
    });
};

mongoose.model('Wall', WallSchema);
const Wall = mongoose.model('Wall');
module.exports = Wall;