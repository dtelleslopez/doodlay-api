const config = require('../config');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LocationSchema = new Schema({
    user: { type: Schema.ObjectId, ref: 'User' },
    locations: [{
        date: {
            type: Date,
            default: Date.now
        },
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    }]
});

LocationSchema.statics.addLocation = (user, coordinates) => {
    return new Promise((resolve, reject) => {
        if (!user || !coordinates) resolve();

        Location.findOne({
            user: user
        }).exec((err, location) => {
            if (err) return reject(err);
            else if (!location) {
                location = new Location({
                    user: user,
                    locations: [{
                        coordinates: coordinates
                    }]
                });
            } else {
                location.locations.push({
                    coordinates: coordinates
                });                
            }

            location.save((err) => {
                if (err) return reject(err);
                else return resolve();
            });
        });
    });
};

mongoose.model('Location', LocationSchema);
const Location = mongoose.model('Location');
module.exports = Location;


