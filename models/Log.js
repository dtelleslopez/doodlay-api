const config = require('../config');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LogSchema = new Schema({
    ip: String,
    user: { type: Schema.ObjectId, ref: 'User' },
    date: { type: Date, default: Date.now },
    method: String,
    path: String,
    params: Object
});

mongoose.model('Log', LogSchema);
const Log = mongoose.model('Log');
module.exports = Log;


