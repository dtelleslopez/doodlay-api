const config = require('../config');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const jwt = require('jwt-simple');
const Schema = mongoose.Schema;
const Relation = require('./Relation');
const waterfall = require('promise-waterfall');
const nearLocation = require('../helpers/near-location');
const randomstring = require("randomstring");

const Friend = {
    user: { type: Schema.ObjectId, ref: 'User' },
    date: { type: Date, default: Date.now }
};

const Device = {
    name: String,
    model: String,
    system: String,
    version: String,
    brand: String,
    token: String,
    date: { type: Date, default: Date.now },
    active: { type: Boolean, default: true }
};

const Term = {
    version: String,
    accepted: { type: Boolean, default: false },
    date: { type: Date, default: Date.now }
};

const UserSchema = new Schema({
    name: String,
    surname: String,
    username: String,
    email: { type: String, unique: true },
    email_confirm: Boolean,
    email_token: String,
    password: { type: String },
    password_token: String,
    image: { type: Schema.ObjectId, ref: 'Image' },
    date: { type: Date, default: Date.now },
    tags: [{ type: Schema.ObjectId, ref: 'Tag' }],
    about: String,
    provider: String,
    locale: String,
    region_code: String,
    devices: [Device],
    near: {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    },
    location: [{
        date: {
            type: Date,
            default: Date.now
        },
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    }],
    terms: [Term],
    deleted: { type: Boolean, default: false },
    deleted_token: String
}, { usePushEach: true });

UserSchema.pre('save', function(next) {
    let user = this;

    if (user.isModified('location')) {
        user.near.coordinates = nearLocation(user.location[0].coordinates, 500);
    }

    if (user.isModified('password')) {
        bcrypt.hash(user.password, null, null, (err, hash) => {
            if (err) return next(err);
            user.password = hash;
            return next();
        });
    } else {
        return next();
    }
});

UserSchema.methods.toJSON = function(properties) {
    let user = this.toObject();
    let basics = ['_id', 'name', 'surname', 'username', 'image', 'about'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(user).forEach((key) => {
        if (basics.indexOf(key) === -1) delete user[key];
        else {
            switch (key) {
                case 'location':
                    if (user[key].length) user[key] = user[key][0];
                    else delete user[key];
                    break;
                case 'sports':
                    if (!user[key].length) delete user[key];
                    break;
                case 'near':
                    if (!user[key] || !user[key].coordinates || !user[key].coordinates.length) delete user[key];
                    break;
            }
        }
    });

    return user;
};

UserSchema.methods.createUsername = function(password) {
    let user = this;

    return new Promise((resolve, reject) => {
        let usernames = [];
        let name = user.name.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();

        usernames.push(name);

        if (user.surname) {
            let surname = user.surname.replace(/[^a-zA-Z0-9]/g, '').toLowerCase();
            usernames.push(name + surname);
            usernames.push(name + Math.floor((Math.random() * 10) + 1));
            usernames.push(name + surname + Math.floor((Math.random() * 10) + 1));
            usernames.push(name + surname + Math.floor((Math.random() * 10000) + 1));
        } else {
            usernames.push(name + Math.floor((Math.random() * 10) + 1));
        }

        User.find({ username: { $in: usernames } }).exec((err, users) => {
            if (err || !users) return resolve(usernames[0]);
            users = users.map((user) => { return user.username });

            let d = new Date();
            user.username = d.getTime();

            for (let i = 0; i < usernames.length; i++) {
                if (users.indexOf(usernames[i]) === -1) {
                    user.username = usernames[i];
                    break;
                }
            }

            resolve(user);
        });
    });
};

UserSchema.methods.isValidPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.createToken = function() {
    let user = this;
    let today = new Date();
    let payload = {
        sub: user._id,
        exp: today.setMonth(today.getMonth() + 6)
    };

    return jwt.encode(payload, config.secret);
};

UserSchema.methods.getRelation = function(user) {
    let friend = this;

    return new Promise((resolve, reject) => {
        Relation.findOne({
            to: { $in: [user, friend._id] },
            from: { $in: [user, friend._id] }
        }).exec((err, relation) => {
            if (err) reject(err);
            else resolve(relation);
        });
    });
};

UserSchema.methods.getRelations = function(users) {
    let friend = this;

    return new Promise((resolve, reject) => {
        if (!users || !users.length) resolve();

        let relations = [];
        let promises = users.map((user) => {
            return () => {
                return new Promise((resolve, reject) => {
                    Relation.findOne({
                        $and: [{
                            $or: [{
                                to: friend._id,
                                from: user
                            }, {
                                to: user,
                                from: friend._id
                            }]
                        }]
                    }).exec((err, relation) => {
                        if (err) reject(err);
                        else if (!relation) {
                            relations.push({ user: user });
                            resolve();
                        } else {
                            relations.push({ user: user, relation: relation.status });
                            resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                resolve(relations);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

UserSchema.methods.getAcceptedRelations = function(users) {
    let friend = this;

    return new Promise((resolve, reject) => {
        if (!users || !users.length) resolve();

        let relations = [];
        let promises = users.map((user) => {
            return () => {
                return new Promise((resolve, reject) => {
                    Relation.findOne({
                        $and: [{
                            $or: [{
                                to: friend._id,
                                from: user,
                                status: config.relation.status.accepted
                            }, {
                                to: user,
                                from: friend._id,
                                status: config.relation.status.accepted
                            }]
                        }]
                    }).exec((err, relation) => {
                        if (err) reject(err);
                        else if (!relation) resolve();
                        else {
                            relations.push(user);
                            resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                resolve(relations);
            })
            .catch((err) => {
                reject(err);
            });
    });
};

UserSchema.methods.getFriends = function() {
    let user = this;

    return new Promise((resolve, reject) => {
        Relation.find({
            $and: [{
                $or: [{
                    from: user,
                    status: config.relation.status.accepted
                }, {
                    to: user,
                    status: config.relation.status.accepted
                }]
            }]
        }).exec((err, relations) => {
            if (err) reject(err);
            else resolve(relations);
        });
    });
};

UserSchema.methods.getFriendsPopulated = function() {
    let user = this;

    return new Promise((resolve, reject) => {
        Relation.find({
                $and: [{
                    $or: [{
                        from: user,
                        status: config.relation.status.accepted
                    }, {
                        to: user,
                        status: config.relation.status.accepted
                    }]
                }]
            })
            .populate('from to')
            .exec((err, relations) => {
                if (err) reject(err);
                else resolve(relations);
            });
    });
};

UserSchema.methods.getFriendsAndPending = function() {
    let user = this;

    return new Promise((resolve, reject) => {
        Relation.find({
                $and: [{
                    $or: [{
                        from: user,
                        status: {
                            $in: [config.relation.status.pending, config.relation.status.accepted]
                        }
                    }, {
                        to: user,
                        status: {
                            $in: [config.relation.status.pending, config.relation.status.accepted]
                        }
                    }]
                }]
            }).populate('from to')
            .exec((err, relations) => {
                if (err) reject(err);
                else resolve(relations);
            });
    });
};

UserSchema.methods.addDevice = function(device) {
    let user = this;

    if (!device || !device.token || device.token === '') return user;
    else {
        let insertDevice = true;

        for (let i = user.devices.length - 1; i >= 0; i--) {
            if (user.devices[i].token === device.token) {
                user.devices[i].active = true;
                insertDevice = false;
            }
        }

        if (insertDevice) {
            user.devices.unshift({
                name: device.name,
                model: device.model,
                system: device.system,
                version: device.version,
                brand: device.brand.toLowerCase(),
                token: device.token
            });
        }

        return user;
    }
};

UserSchema.methods.getDeviceByToken = function(device) {
    let user = this;

    if (device && device.token && device.token !== '') {
        for (let i = user.devices.length - 1; i >= 0; i--) {
            if (user.devices[i].token === device.token) return user.devices[i];
        }
    }

    return;
};

UserSchema.methods.acceptTerms = function(version) {
    let user = this;

    return new Promise((resolve, reject) => {
        let versions = user.terms.map((term) => {
            return term.version;
        });

        if (versions.indexOf(version) > -1) {
            return resolve();
        }
        else {
            let term = {
                version: version,
                accepted: true
            }

            user.terms = user.terms || [];
            user.terms.push(term);
            user.save((err) => {
                if (err) return reject(err);
                return resolve();
            });
        }
    });
};

UserSchema.statics.toJSON = function(user, properties) {
    let basics = ['_id', 'name', 'surname', 'username', 'image', 'about'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(user).forEach((key) => {
        if (basics.indexOf(key) === -1) delete user[key];
        else {
            switch (key) {
                case 'location':
                    if (user[key].length) user[key] = user[key][0];
                    else delete user[key];
                    break;
                case 'sports':
                    if (!user[key].length) delete user[key];
                    break;
            }
        }
    });

    return user;
};

UserSchema.statics.getUserById = (_id) => {
    return new Promise((resolve, reject) => {
        User.findOne({ 
            _id: _id,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err || !user) return reject(err);
            return resolve(user);
        });
    });
};

UserSchema.statics.getUserByUsername = (username) => {
    return new Promise((resolve, reject) => {
        User.findOne({ 
            username: username,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err || !user) return reject(err);
            return resolve(user);
        });
    });
};

UserSchema.statics.getUsersById = (_ids) => {
    return new Promise((resolve, reject) => {
        if (!_ids.length) resolve([]);

        User.find({
            _id: { $in: _ids },
            deleted: { $ne: true }
        }).exec((err, users) => {
            if (err) return reject(err);
            return resolve(users);
        });
    });
};

UserSchema.statics.confirmEmail = (email_token) => {
    return new Promise((resolve, reject) => {
        User.findOne({ 
            email_token: email_token,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err || !user) return reject(err);
            user.email_confirm = true;
            user.email_token = undefined;
            user.save((err) => {
                if (err) return reject(err);
                return resolve();
            });
        });
    });
};

UserSchema.statics.changePassword = (password, password_security, password_token) => {
    return new Promise((resolve, reject) => {
        if (password !== password_security) return reject();

        User.findOne({ 
            password_token: password_token,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err) return reject(err);
            else if (!user || user.provider) return reject();

            user.password = password;
            user.password_token = undefined;
            user.save((err, user) => {
                if (err) return reject(err);
                else resolve(user);
            });
        });
    });
};

UserSchema.statics.forgotPassword = (email) => {
    return new Promise((resolve, reject) => {
        if (!email) return reject();

        User.findOne({ 
            email: email,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err) return reject(err);
            else if (!user || user.provider) return reject();

            user.password_token = randomstring.generate();
            user.save((err, user) => {
                if (err) return reject(err);
                else resolve(user);
            });
        });
    });
};

UserSchema.statics.deleteUser = (user) => {
    return new Promise((resolve, reject) => {
        if (!user) return reject();

        User.findOne({ 
            _id: user._id,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err) return reject(err);
            else if (!user) return reject();

            user.deleted_token = randomstring.generate();
            user.save((err, user) => {
                if (err) return reject(err);
                else resolve(user);
            });
        });
    });
};

UserSchema.statics.confirmDelete = (deleted_token) => {
    return new Promise((resolve, reject) => {
        User.findOne({ 
            deleted_token: deleted_token,
            deleted: { $ne: true }
        }).exec((err, user) => {
            if (err || !user) return reject(err);
            user.deleted = true;
            user.deleted_token = undefined;
            user.save((err) => {
                if (err) return reject(err);
                return resolve();
            });
        });
    });
};

mongoose.model('User', UserSchema);
const User = mongoose.model('User');
module.exports = User;