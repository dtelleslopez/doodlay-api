const config = require('../config');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Message = {
    user: { type: Schema.ObjectId, ref: 'User' },
    date: Date,
    received: { type: Date, default: Date.now },
    message: String,
    readed: [{
        type: Schema.ObjectId,
        ref: "User"
    }]
};

const RelationSchema = new Schema({
    from: { type: Schema.ObjectId, ref: 'User' },
    to: { type: Schema.ObjectId, ref: 'User' },
    date: { type: Date, default: Date.now },
    status: { type: String, default: 'pending' },
    messages: [Message]
});

RelationSchema.methods.toJSON = function(properties) {
    let relation = this.toObject();
    let basics = ['_id', 'from', 'to', 'status'];

    if (properties && properties.length) basics = basics.concat(properties);
    Object.keys(relation).forEach((key) => {
        if (basics.indexOf(key) === -1) delete relation[key];
    });

    return relation;
};

mongoose.model('Relation', RelationSchema);
const Relation = mongoose.model('Relation');
module.exports = Relation;