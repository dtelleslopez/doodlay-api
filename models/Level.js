const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LevelSchema = new Schema({
    name: String
});

LevelSchema.statics.getLevelByName = (level) => {
    return new Promise((resolve, reject) => {
        if (!level) resolve();
        else {
            Level.findOne({ name: level }).exec((err, level) => {
                if (err || !level) return reject(err);
                else return resolve(level);
            });
        }
    });
};

mongoose.model('Level', LevelSchema);
const Level = mongoose.model('Level');
module.exports = Level;