#!/bin/bash

cd /home/ec2-user/doodlay-api
pm2 start ecosystem.json --env production
pm2 update