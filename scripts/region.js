const config = require('../config');
const mongoose = require('mongoose');
const User = require('../models/User');
const waterfall = require('promise-waterfall');

mongoose.connect(config.mongo, (err) => {
    User.find({
        region_code: null
    }).exec((err, users) => {
        let promises = users.map((user, key) => {
            return () => {
                return new Promise((resolve, reject) => {
                    user.region_code = 'ES';
                    user.save((err) => {
                        if (err) {
                            console.log('Error with user:', user._id);
                            reject(err);
                        } else {
                            console.log(`User ${key + 1}/${users.length} done!`);
                            resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                console.log('All done!');
            })
            .catch((err) => {
                console.log('Error:', err);
            });
    });
});