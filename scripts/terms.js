const config = require('../config');
const mongoose = require('mongoose');
const User = require('../models/User');
const waterfall = require('promise-waterfall');
const Terms = require('../data/Terms');

mongoose.connect(config.mongo, (err) => {
    User.find({
        terms: null
    }).exec((err, users) => {
        let version = Object.keys(Terms.en);
        version = version[version.length - 1];

        let promises = users.map((user, key) => {
            return () => {
                return new Promise((resolve, reject) => {
                    let term = {
                        version: version,
                        accepted: true,
                        date: user.date
                    }

                    user.terms = user.terms || [];
                    user.terms.push(term);
                    user.save((err) => {
                        if (err) {
                            console.log('Error with user:', user._id);
                            reject(err);
                        } else {
                            console.log(`User ${key + 1}/${users.length} done!`);
                            resolve();
                        }
                    });
                });
            };
        });

        waterfall(promises).then(() => {
                console.log('All done!');
            })
            .catch((err) => {
                console.log('Error:', err);
            });
    });
});