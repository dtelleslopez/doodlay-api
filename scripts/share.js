const config = require('../config');
const mongoose = require('mongoose');
const Activity = require('../models/Activity');
const waterfall = require('promise-waterfall');
const randomstring = require('randomstring');

mongoose.connect(config.mongo, (err) => {
    Activity.find().select('share').exec((err, shares) => {
        shares = shares.map((activity) => {
            return activity.share;
        });

        Activity.find({
            share: null
        }).exec((err, activities) => {
            let promises = activities.map((activity, key) => {
                return () => {
                    return new Promise((resolve, reject) => {
                        let share = randomstring.generate(8);
                        while (shares.indexOf(share) > -1) {
                            share = randomstring.generate(8);
                        }

                        activity.share = share;
                        activity.save((err, activity) => {
                            if (err) {
                                console.log('Error with activity:', activity._id);
                                reject(err);
                            } else {
                                console.log(`Activity ${key + 1}/${activities.length} done!`);
                                shares.push(share);
                                resolve();
                            }
                        });
                    });
                };
            });

            waterfall(promises).then(() => {
                    console.log('All done!');
                })
                .catch((err) => {
                    console.log('Error:', err);
                });
        });
    });
});