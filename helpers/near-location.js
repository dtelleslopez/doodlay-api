'use strict';

module.exports = function(coordinates, radius) {
    let x0 = coordinates[0];
    let y0 = coordinates[1];

    let rd = radius/111300;

    let u = Math.random();
    let v = Math.random();

    let w = rd * Math.sqrt(u);
    let t = 2 * Math.PI * v;
    let x = w * Math.cos(t);
    let y = w * Math.sin(t);

    let xp = x/Math.cos(y0);

    return [xp + x0, y+y0];
};