const config = require('../config');
const nodemailer = require('nodemailer');
const fs = require('fs');
const ejs = require('ejs');
const Locale = require('./locale');
const dirname = __dirname + '/../templates/mail/';
const smtpTransport = nodemailer.createTransport({
    host: config.mailer.host,
    port: config.mailer.port,
    secure: config.mailer.secure,
    tls: {
        rejectUnauthorized: false
    },
    auth: {
        user: config.mailer.user,
        pass: config.mailer.pass
    }
});

let templates = {};
let templateFiles = fs.readdirSync(dirname);
templateFiles.forEach((file) => {
    if (!fs.lstatSync(dirname + '/' + file).isDirectory()) {
        let filename = file.split('.')[0];
        templates[filename] = fs.readFileSync(dirname + '/' + file, 'utf-8');
    }
});

exports.send = (locale, name, data) => {
    locale = Locale(locale);
    
    data.environment = config.web.endpoint + locale + (!process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? '/dev/' : '/');

    let template = require('../data/Templates')[name](data);
    template = template[locale];

    data = Object.assign(data, template);

    let html = ejs.render(templates[name], data);
    let mailOptions = {
        from: '"Doodlay" <hello@doodlay.co>',
        to: data.email,
        subject: data.subject,
        html: html
    };

    smtpTransport.sendMail(mailOptions, (err) => {
        if (err) reject(err);
        else resolve();
    });
};