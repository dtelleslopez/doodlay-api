'use strict';

const config = require('../config');

module.exports = function(locale) {
	if (!locale) locale = config.default.locale;
	
    locale = locale.split('_')[0];
    if (locale !== 'es' && locale !== 'en') locale = config.default.locale.split('_')[0];

    return locale;
};