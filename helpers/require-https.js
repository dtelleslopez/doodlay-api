'use strict';

const config = require('../config');

module.exports = function(req, res, next) {
	if (req.headers['x-forwarded-proto'] !== 'https') return res.redirect('https://' + req.get('host') + req.url);
	return next();
};