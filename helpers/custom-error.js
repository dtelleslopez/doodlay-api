'use strict';

module.exports = function CustomError(message, status, error, extra) {
    Error.captureStackTrace(this, this.constructor);
    if (extra) this.error = extra;
    if (error) console.error(error);
    this.message = message;
    this.status = status || 500;
};

require('util').inherits(module.exports, Error);