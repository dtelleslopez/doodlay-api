'use strict';

const Slack = require('slack-node');
const config = require('../config');
const Norris = {
    username: 'Chuck Norris',
    channel: '#logs',
    icon_emoji: 'http://www.elsbastards.cat/files/2015/09/Norris04.jpg',
    slack: new Slack()
};

Norris.slack.setWebhook('https://hooks.slack.com/services/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');

module.exports = function(type, data, callback) {
    var attachment = null;

    if (!process.env.NODE_ENV) return;

    switch (type) {
        case config.norris.start_server:
            attachment = [{
                color: '#ff0000',
                title: `¡Patada voladora al servidor ${process.env.NODE_ENV}!`,
                text: 'Servidor iniciado correctamente'
            }];
            break;
        case config.norris.create_activity:
            attachment = [{
                'color': '#0080ff',
                'title': `¡Nueva actividad creada ${process.env.NODE_ENV}!`,
                'text': data.name
            }];
            break;
        case config.norris.new_user:
            attachment = [{
                'color': "#0000cc",
                'title': `¡Nuevo usuario registrado ${process.env.NODE_ENV}!`,
                'text': data.surname ? data.name + " " + data.surname : data.name
            }];
            break;
        default:
            break;
    }

    Norris.slack.webhook({
        username: Norris.username,
        channel: Norris.channel,
        icon_emoji: Norris.icon_emoji,
        attachments: attachment
    }, (err, response) => {
        if (callback) callback();
    });
};