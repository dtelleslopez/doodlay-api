const config = require('../config');
const request = require('request');

exports.sendPostRequest = (endpoint, json) => {
    return new Promise((resolve, reject) => {
        request({
            url: config.notifications.host + endpoint,
            method: 'POST',
            json: true,
            body: json
        }, (error, response, body) => {
        	resolve();
        });
    });
};