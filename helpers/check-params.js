'use strict';

module.exports = function(body, params) {
	for (let i = 0; i < params.length; i++) {
		if (body[params[i]] === undefined || body[params[i]] === null) return({ message: 'Bad request. Parameter ' + params[i] + ' not provided', error: 400 });
    }

    return;
};